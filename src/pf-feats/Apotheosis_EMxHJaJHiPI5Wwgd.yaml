_id: EMxHJaJHiPI5Wwgd
_key: '!items!EMxHJaJHiPI5Wwgd'
img: systems/pf1/icons/feats/alignment-channel.jpg
name: Apotheosis
system:
  description:
    value: >-
      <p><em>You are marked by fate as a future deity--even before this destiny
      is realized, fate bends to your
      will.</em></p><p><strong>Prerequisites</strong>: You must have had direct
      contact with a deity or godlike being, or have been risen from the dead at
      the personal behest of a deity or godlike
      being.</p><p><strong>Benefits</strong>: You gain a +2 bonus on
      Constitution checks to stabilize while dying. Anytime you or an ally who
      worships the god that contacted you casts augury or a similar effect, your
      chance of receiving a meaningful reply is increased by 5% (to a maximum of
      95%).</p><p><strong>Goal</strong>: You must be acknowledged by another
      divine being (directly or through a representative) as a peer, even if
      only a minor one.</p><p><strong>Completion Benefit<br /></strong>Once per
      day as an immediate action, you can cause any die roll made by a creature
      within 100 feet of you to be rerolled. You choose which result you prefer.
      You must decide to use this ability after the first roll is made but
      before the results are revealed by the GM.</p><p><strong>Suggested
      Traits</strong><br/> Called, Fortunate, Inspiring.</p><h2>Possible
      Apotheosis Quests</h2><p><strong>&nbsp; d8 Quest</strong></p><ol><li>You
      seek to prove your worth and acquire followers who will exalt you as a
      living ideal or preeminent martyr, like Kurgess (page 229).</li><li>Omens
      indicate that if you give all in service to your deity, you will become
      his demigod servant, much as Hanspur was deified by Gozreh (page
      229).</li><li>You believe the mad whispers of those cultists who claim
      that if one were to travel far enough into the Dark Tapestry, one might
      tap into the divine power of the Great Old Ones (page 235).</li><li>The
      Test of the Starstone is among the best-known paths to divinity; you
      adventure in preparation to take the test yourself and join the ranks of
      ascended deities (page 39).</li><li>While studying the mysteries of
      Ancient Osirion, you stumble across clues to the mythic divination that
      elevated Nethys from mortal king to deity (page 223).</li><li>You
      challenge yourself in the ways described in Irori&rsquo;s holy book,
      Unbinding the Fetters, as you seek the mental and physical perfection that
      will lead you to divinity (page 222).</li><li>By tracking down and
      confronting one of the legendary titans of Elysium, you hope to unlock the
      secret to achieving divinity in the celestial realms (page
      243).</li><li>You have learned that one of your ancestors was a deity
      (such as the prolific Cayden Cailean) and you now quest to get his
      attention and earn a similar station (page 219).</li></ol>
  tags:
    - Story
type: feat

