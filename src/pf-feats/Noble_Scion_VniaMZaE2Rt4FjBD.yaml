_id: VniaMZaE2Rt4FjBD
_key: '!items!VniaMZaE2Rt4FjBD'
img: icons/equipment/head/crown-gold-red.webp
name: Noble Scion
system:
  description:
    value: >-
      <p><em>You are a member of a proud noble family, whether or not you remain
      in good standing with your
      family.</em></p><p><strong>Prerequisites</strong>: Cha 13, must be taken
      at 1st level.</p><p><strong>Benefits</strong>: You gain a +2 bonus on all
      Knowledge (nobility) checks, and that chosen Knowledge skill is always
      considered a class skill for you.</p><p>When you select this feat, choose
      one of the benefits listed below that matches the flavor of your noble
      family. Work with your GM to ensure that your choice is
      appropriate.</p><hr /><p><strong>Scion of the Arts</strong>: You gain a +1
      bonus on all Perform checks, and Perform is always a class skill for you.
      If you have the bardic performance ability, you can use that ability for
      an additional 3 rounds per day.</p><p><strong>Scion of Lore</strong>: You
      gain a +1 bonus on all Knowledge skills in which you have at least 1
      rank.</p><p><strong>Scion of Magic</strong>: You gain one of the following
      languages as a bonus language<br />Abyssal, Aklo, Celestial, Draconic,
      Infernal, or Sylvan. Once per day, as a free action, you can gain a +2
      bonus on any Spellcraft check you make. You must spend the free action to
      gain this bonus before you make the check.</p><p><strong>Scion of
      Peace</strong>: Whenever you take 10 on a Wisdom-based skill, treat the
      result as if you had rolled a 13 instead of a 10.</p><p><strong>Scion of
      War</strong>: You use your Charisma modifier to adjust Initiative checks
      instead of your Dexterity modifier.</p><p><strong>Note</strong>: Noble
      houses throughout Golarion train their courts to focus on the ideals and
      skills most appropriate for their given region. The Noble Scion feat
      (Pathfinder Campaign Setting: The Inner Sea World Guide 288) provides
      benefits to noble characters. Presented below are a number of benefits
      that can be taken in place of the standard selection of benefits provided
      by the feat.</p><hr /><p><strong>Scion of Absalom</strong>: You gain one
      of the following languages as a bonus language: Dwarven, Elven, Giant,
      Gnome, Halfling, or Orc. You gain a +1 bonus on Diplomacy checks when
      interacting with a creature that shares this bonus language.
      </p><p><strong>Scion of Brevoy</strong>: You gain a +2 bonus on Bluff
      checks against other nobles or members of a noble court.
      </p><p><strong>Scion of Cheliax</strong>: You gain a +1 bonus on Knowledge
      (planes) checks and treat your caster level as 1 higher when attempting to
      bind or call a devil.</p><p><strong>Scion of Goka</strong>: Choose either
      Sleight of Hand or Stealth. While you are in a small city or larger
      settlement, you can take 10 with the chosen skill, even while rushed or
      threatened.</p><p><strong>Scion of Highhelm</strong>: You use your
      Constitution modifier instead of your Charisma modifier for the purpose of
      the Leadership feat.</p><p><strong>Scion of Irrisen</strong>: You reduce
      the amount of nonlethal damage you take from exposure to cold by 4
      (minimum 0).</p><p><strong>Scion of Jalmeray</strong>: Once per day, you
      can reroll a save against a psychic spell and gain a +2 bonus on that
      reroll.</p><p><strong>Scion of Katapesh</strong>: Once per day, you can
      attempt a Diplomacy check to reduce the cost of an item by 10% (DC = 15, +
      1 for every 500 gp of the item's base cost). The item must have a market
      value of 5,000 gp or less.</p><p><strong>Scion of Kyonin</strong>: You use
      your Dexterity modifier instead of your Charisma modifier for the purpose
      of the Leadership feat.</p><p><strong>Scion of Nidal</strong>: You gain
      low-light vision. If you already have low-light vision, you instead gain a
      +2 bonus on Perception checks in areas of dim light. </p><p><strong>Scion
      of Numeria</strong>: You gain a +1 bonus on skill checks when interacting
      with technology (Pathfinder Campaign Setting<br />Technology Guide 5) and
      a +2 bonus on damage rolls against creatures of the robot
      subtype.</p><p><strong>Scion of Osirion</strong>: You reduce the amount of
      nonlethal damage you take from heat exposure by 2 (minimum 0).
      Additionally, you require only a gallon of water per day to avoid
      dehydration in hot climates.</p><p><strong>Scion of Qadira</strong>: Once
      per day, when attempting to negate an attack with Mounted Combat, you can
      roll twice and take the better result. Ride is a class skill for you.
      </p><p><strong>Scion of the River Kingdoms</strong>: Whenever you take 10
      on Handle Animal, Knowledge (nature), Perception, Ride, or Survival
      checks, treat the result as if you had rolled a 13 instead of a 10.
      </p><p><strong>Scion of Taldor</strong>: You gain a +2 bonus on
      Charisma-based checks when interacting with nobles or members of a noble
      court.</p><p><strong>Scion of Ustalav</strong>: Once per day as a free
      action, you can suppress any fear effects affecting you for a number of
      rounds equal to half your Hit Dice (minimum 1) even if you could not
      normally take actions due to your fear. Knowledge (religion) is a class
      skill for you.</p>
  tags:
    - General
type: feat

