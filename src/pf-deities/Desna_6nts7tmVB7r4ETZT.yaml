_id: 6nts7tmVB7r4ETZT
_key: '!journal!6nts7tmVB7r4ETZT'
content: >-
  <h1>Desna</h1><h2>Song of the Spheres</h2><b>Source</b> <a><i>Inner Sea Gods
  pg. 44</i></a><br /><b>Pathfinder Wiki</b> <a
  href="http://www.pathfinderwiki.com/wiki/Desna">Desna</a><h3>Details</h3><p><b>Alignment</b>
  CG<br /><b>Pantheon</b> Core Deities<br /><b>Other Pantheons</b> Deities of
  Tian Xia, Elven Deities, Gnomish Deities, Halfling Deities, Sandpoint
  Pantheon<br /><b>Areas of Concern</b> Dreams, luck, stars, travelers<br
  /><b>Domains</b> Chaos, Good, Liberation, Luck, Travel<br /><b>Subdomains</b>
  Azata (Chaos), Azata (Good), Curse, Exploration, Fate, Freedom, Revolution<br
  /><i>* Requires the <u>Acolyte of Apocrypha</u> trait.</i><br /><b>Favored
  Weapon</b> Starknife<br /><b>Symbol</b> Butterfly<br /><b>Sacred Animal(s)</b>
  Butterfly<br /><b>Sacred Color(s)</b> Blue, white<br
  /></p><h3>Obedience</h3><p>Dance in a random pattern beneath the light of the
  stars, trusting in the guidance of destiny. If no stars are currently visible,
  softly sing or chant all of the names of stars that you know as you perform
  your dance. Let your mind expand and turn your thoughts away from where your
  feet might land, allowing your steps to fall where chance wills. When the
  dance feels complete, cease dancing. Ponder the steps you took and the
  position in which you stopped, and consider what portents these subtle clues
  might hold for the future. Gain a +1 luck bonus on initiative checks and a +4
  sacred bonus on Perception checks made to determine whether you are aware of
  opponents just before a surprise round.</p><h3>Divine
  Gift</h3><p><b>Source</b> <a><i>Planar Adventures pg. 75</i></a><br
  /><i>Nethys Note: See here for details on how to gain a Divine Gift</i><br
  />The character gains 20 luck points that can be spent at any time as a free
  action to increase the rolled result of a single d20 roll on a one-to-one
  basis. This ability can be used as many times as the character wishes as long
  as she retains luck points to spend, but only once per roll. These luck points
  can be spent after the result of the roll is determined, but must be spent
  immediately or the opportunity to adjust the luck of that roll is
  lost.</p><h3>On Golarion</h3><p><b>Centers of Worship</b> Kyonin, Lands of the
  Linnorm Kings, Nidal, Numeria, River Kingdoms, Ustalav, Varisia<br
  /><b>Nationality</b> Varisian</p><h2>Boons - Deific Obedience</h2><h3>
  Evangelist</h3><p><strong>Source</strong>: Inner Sea Gods pg. 44<br /><b>1:
  Traveler's Tricks (Sp)</b> <i>longstrider </i>3/day, <i>darkvision </i>2/day,
  or <i>phantom steed </i>1/day<br /><b>2: Starlit Caster (Su)</b> Over time you
  have learned to focus your magical power to better damage agents of evil. You
  add your Charisma bonus on your concentration checks, as well as on your
  caster level checks to overcome spell resistance. In addition, when you stand
  in starlight and cast a spell that deals hit point damage, you can have it
  deal an extra 2d6 points of damage. This bonus damage is untyped, and
  manifests as a glowing aura of starlight around the spell's original
  effect.<br /><b>3: Starry Eyes (Su)</b> You gain darkvision with a range of 60
  feet. If you already have darkvision with a range of 60 feet or greater, the
  range of your darkvision instead increases by 10 feet. This extension applies
  only to natural, permanent darkvision, not to darkvision that is granted by
  spells or other effects. In addition, while you stand in starlight the range
  of your spells (other than spells with the range of 'personal' or 'touch')
  increases as though your caster level were 1 level
  higher.</p><h3>Exalted</h3><p><b>Source </b><a><i>Inner Sea Gods pg.
  44</i></a><br /><b>1: Slumberer (Sp)</b> <i>sleep </i>3/day, <i>silence
  </i>2/day, or <i>deep slumber </i>1/day<br /><b>2: Splendorous Ally (Sp)</b>
  Once per day as a standard action, you can summon a lillend azata. You gain
  telepathy with the lillend to a range of 100 feet. The lillend follows your
  commands perfectly for 1 minute for every Hit Die you possess before vanishing
  back to its home on Elysium. The lillend doesn't follow any commands that
  would cause it to take evil or overly lawful actions, and the creature could
  even attack you if the command is particularly egregious.<br /><b>3: Blast of
  Motes (Su)</b> Whenever you channel positive energy to heal living creatures,
  a shower of starry motes cascades over all of the living creatures in the area
  of effect. These motes do not impede vision or stealth attempts, nor do they
  reveal invisible creatures, but they infuse the targets with divine luck.
  Targets of this ability reduce any miss chance they face by 10%. In addition,
  when targets of this ability roll damage dice, they may treat all 1s as 2s.
  The motes and their effects last for 1 round + 1 round for every 4 Hit Dice
  you have (maximum 6 rounds). If you don't have the ability to channel positive
  energy, you gain the ability to do so once per day as a cleric of a level
  equal to your Hit Dice (maximum 20), but only to heal living creatures.
  Whenever you use this ability, the beneficiaries are showered with starry
  motes, with the effects described
  above.</p><h3>Sentinel</h3><p><strong>Source</strong>: Inner Sea Gods pg.
  44<br /><b>1: Fighting Chance (Sp)</b> <i>entropic shield </i>3/day, <i>blur
  </i>2/day, or <i>displacement </i>1/day<br /><b>2: See through Dreams (Su)</b>
  Desna's blessing allows you to see through half-real and fantastical images.
  You gain a +1 luck bonus on saving throws made against illusion spells and
  effects and against dream-based magic (such as <i>nightmare</i>). This bonus
  increases by 1 for every 4 Hit Dice you possess (maximum +6).<br /><b>3:
  Shooting Star (Ex)</b> You can throw your deity's star-shaped favored weapon
  with great speed and ease. Three times per day, you can make a single ranged
  attack with a starknife as a swift action. To throw a starknife as a swift
  action, you must have it in hand, have sufficient actions available to draw
  the starknife, or have the Quick Draw feat or a similar ability.</p><h2>Divine
  Fighting Technique</h2><h3>Desna's Shooting Star</h3><p><b>Source</b>
  <a><i>Divine Anthology pg. 28</i></a><br />Among the divine fighting manuals
  of the Inner Sea, few are as ancient as <i>Clamor of the Spheres</i>, a
  collection of fighting techniques favored by Desna's faithful. True to its
  name, the manual focuses on interpreting the chaos and sounds of combat, but
  nevertheless provides insightful and downright brilliant methods of defense
  with Desna's favored weapon, using techniques that treat a fight with a
  starknife more as a beautiful dance than a battle.</p><p><b>Optional
  Replacement</b>: A chaotic good bard of at least 2nd level who worships Desna
  can replace a versatile performance with the following initial
  benefit.</p><p><b>Initial Benefit</b>: You can add your Charisma bonus to
  attack rolls and damage rolls when wielding a starknife. If you do so, you
  don't modify attack rolls and damage rolls with your starknife with your
  Strength modifier, your Dexterity modifier (if you have Weapon Finesse), or
  any other ability score (if you have an ability that allows you to modify
  attack rolls and damage rolls with that ability score).</p><p><b>Advanced
  Prerequisites</b>: Dex 17; Divine Fighting Technique; Point-Blank Shot; Rapid
  Shot; base attack bonus +11 or Sleight of Hand 11 ranks.</p><p><b>Optional
  Replacement</b>: A chaotic good bard of at least 10th level who worships Desna
  can replace a versatile performance with the following advanced benefit
  without meeting its prerequisites.</p><p><b>Advanced Benefit</b>: You can
  impart a powerful spin to a thrown starknife so that multiple blades strike
  the target rather than just a single blade of the four, dealing extra damage
  with the other blades. As a full-round action, you can make a single attack
  with a thrown starknife, rolling 1d4 to determine how many effective strikes
  you gain with the attack (if you roll a 1, then only one blade strikes). If
  the attack hits, all of the effective strikes damage the target. Apply
  precision-based damage (such as sneak attack damage) and critical hit damage
  only once for this attack. The damage bonus from your appropriate ability
  score modifier applies to each strike, as do other damage bonuses, such as a
  bard's inspire competence bonus. Damage reduction and resistances apply
  separately to each strike.</p><h2>For Followers of
  Desna</h2><h3>Archetypes</h3><p>Guide (Ranger), Stargazer (Oracle), World
  Walker (Druid)</p><h3>Feats</h3><p>Butterfly's Sting, Cosmic Gate, Guided
  Star, Improved Stellar Wanderer, Lady Luck's Guidance, Stellar Wanderer,
  Trailblazing Channel</p><h3>Magic Items - Altars</h3><p>Altar of
  Desna</p><h3>Magic Items - Armor</h3><p>Starsong Mail</p><h3>Magic Items -
  Rings</h3><p>Ring of Stairs and Stars</p><h3>Magic Items - Staves</h3><p>Staff
  of Slumber</p><h3>Magic Items - Weapons</h3><p>Shooting Starknife</p><h3>Magic
  Items - Wondrous Items</h3><p>Cloak of the Night Sky, Desna's Coin, Dream
  Candle, Dreamwing Cape, Necklace of Netted Stars, Robe of Stars, Starfaring
  Robes, Stone of Good Luck (Luckstone)</p><h3>Monsters</h3><p>Night Monarch
  (Herald), Thyrlien</p><h3>Prestige Classes</h3><p>Sphere Singer,
  Spherewalker</p><h3>Spells</h3><p>Beacon of Luck, Dream, Dream Feast, Find the
  Path, Guiding Star, Haze of Dreams, Traveling Dream, Wandering Star
  Motes</p><h3>Traits</h3><p>Faithful Artist, Follower of the Stars, Good
  Dreams, Starchild, Stoic Optimism, Thrill-Seeker</p><h2>Unique Spell
  Rules</h2><b>Source </b><a><i>Inner Sea Gods pg. 51</i></a><br
  /><h3>Cleric/Warpriest</h3><p><i>Dream</i> can be prepared as a 5th-level
  spell<br /></p><h3>Druid</h3><p><i>Dream</i> can be prepared as a 5th-level
  spell<br /></p><h3>Ranger</h3><p><i>Dream</i> can be prepared as a 4th-level
  spell</p><h2>Unique Summon Rules</h2><b>Source </b><a><i>Pathfinder #2: The
  Skinsaw Murders pg. 72</i></a><br /><b>Summon Monster II</b>: Lyrakien - CG<br
  /><b>Summon Monster III</b>: Star Monarch - CG [stats as a giant eagle]<br
  /><b>Summon Monster VII</b>: Young Brass Dragon - CG
  (extraplanar)</p><p><strong>Source</strong>: Monster Summoner's Handbook pg.
  30<br /><b>Summon Monster II</b>: Pseudodragon - CG<br /><br
  /><div><div><iframe> </iframe></div>
   </div>
name: Desna
pages: []

