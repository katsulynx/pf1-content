_id: RXIgGYdEyFEZPXDj
_key: '!journal!RXIgGYdEyFEZPXDj'
content: >-
  <h1>Xoveron</h1><h2>The Horned Prince</h2><b>Source</b> <a><i>Inner Sea Gods
  pg. 320</i></a><br /><b>Pathfinder Wiki</b> <a
  href="http://www.pathfinderwiki.com/wiki/Xoveron">Xoveron</a><h3>Details</h3><p><b>Alignment</b>
  CE<br /><b>Pantheon</b> Demon Lords<br /><b>Areas of Concern</b> Gargoyles,
  gluttony, ruins<br /><b>Domains</b> Chaos, Earth, Evil, Strength<br
  /><b>Subdomains</b> Caves, Demon (Chaos), Demon (Evil), Entropy, Ferocity,
  Petrification*<br /><i>* Requires the <u>Acolyte of Apocrypha</u>
  trait.</i><br /><b>Favored Weapon</b> Ranseur<br /><b>Symbol</b> 5-horned
  gargoyle skull<br /><b>Sacred Animal(s)</b> Boar<br /><b>Sacred Color(s)</b>
  Black, brown<br /></p><h3>Obedience</h3><p>Perch atop a high outcrop and look
  out over the surrounding terrain. If the outcrop is in an uninhabited area,
  you need do nothing more but wait for an hour, but if the outcrop is in an
  inhabited area (such as a city), no passersby should realize you are a living
  thing-any who do must be slain before the hour's end. Gain a +4 profane bonus
  on saving throws against effects that cause sickness, nausea, fatigue, or
  exhaustion.</p><h2>Boons - Demonic Obedience</h2><h3>Demoniac</h3><p><b>Source
  </b><a><i>Book of the Damned - Volume 2: Lords of Chaos pg. 27</i></a><br
  /><b>1: Gargoyle's Gift (Sp)</b> <i>sanctuary </i>3/day, <i>shatter </i>2/day,
  or <i>vampiric touch </i>1/day<br /><b>2: Glutton's Feast (Sp)</b> You can
  cast <i>heroes' feast</i> once per day. The food created by this effect
  consists of raw or rotting meat and rancid milk. Those who partake of this
  feast consume their food shockingly fast, as if they were starving-it takes
  only 1 minute to gain the effects of this spell. Nonworshipers of Xoveron must
  make a Fortitude save (16 + your Charisma modifier) to avoid being sickened by
  the feast for 6 hours (though all other benefits of the feast still apply).
  This ability is the equivalent of a 7th-level spell.<br /><b>3: Death-Stealing
  Gaze (Su)</b> You gain the death-stealing gaze ability of a nabasu
  (<i>Pathfinder RPG Bestiary </i>64). You can activate this ability as a free
  action, and can use it for up to 3 rounds per day plus an additional number of
  rounds equal to your Constitution bonus-these rounds need not be consecutive.
  The saving throw to resist this gaze is equal to 10 + 1/2 your HD + your
  Charisma modifier. Nabasu demons who gain this boon can use their
  death-stealing gaze at will, regardless of their total number of growth
  points.</p><h2>Boons - Fiendish
  Obedience</h2><h3>Evangelist</h3><p><strong>Source</strong>: Book of the
  Damned pg. 102<br /><b>1: Master of Desolation (Sp)</b> <i>detect secret doors
  </i> 3/day, <i>soften earth and stone</i> 2/day, or <i>meld into stone</i>
  1/day<br /><b>2: Ruinwalker (Sp)</b> The Horned Prince is master of all ruins,
  and his blessing lets you flash instantly from place to place in such desolate
  areas. You can cast <i>dimension door</i> (self only) as a spell-like ability
  a number of times per day equal to your Hit Dice. Both the start and end point
  of your teleportation must be within a single continuous area of ruins
  terrain, such as an ancient dungeon or catastrophe-wracked city. Whether a
  given area constitutes ruins terrain is decided at the GM's discretion.<br
  /><b>3: Wake the Ruined Realm (Sp)</b> At your bidding, the remains of ancient
  civilizations and shattered cities rise up to destroy your foes. Once per day
  as a standard action, you can cause a Large stone statue within 30 feet to
  animate as a stone golem. The golem obeys your commands and remains active for
  up to 1 hour. If the stone statue you target is a statue of Xoveron, the
  resulting stone golem gains the advanced creature
  template.</p><h3>Exalted</h3><p><strong>Source</strong>: Book of the Damned
  pg. 102<br /><b>1: Gargoyle's Gift (Sp)</b> <i>sanctuary </i> 3/day,
  <i>shatter</i> 2/day, or <i>vampiric touch</i> 1/day<br /><b>2: Glutton's
  Feast (Sp)</b> You can cast <i>heroes' feast</i> once per day as a spell-like
  ability. The food created by this effect consists of rancid milk and raw or
  rotting meat. Those who partake of this feast consume their food shockingly
  fast, as if they were starving-it takes only 1 minute to gain the benefits of
  this spell. Those who don't worship Xoveron must succeed at a Fortitude save
  (DC = 16 + your Charisma modifier) or be sickened by the feast for 6 hours
  (though all other benefits of the feast still apply).<br /><b>3:
  Death-Stealing Gaze (Su)</b> You gain the death-stealing gaze ability of a
  nabasu. You can activate this ability as a free action and use it for up to 3
  rounds per day plus a number of additional rounds equal to your Constitution
  modifier-these rounds need not be consecutive, but they must be used in
  1-round increments. All living creatures within 30 feet of you when your
  death-stealing gaze is active must succeed at a Fortitude save (DC = 10 + half
  your Hit Dice + your Charisma modifier) or gain a negative level. A humanoid
  slain in this manner immediately transforms into a ghoul under your control.
  You can create only one ghoul in this manner per round. If multiple humanoids
  die from this ability simultaneously, you choose which of them rises as a
  ghoul. Nabasu demons that gain this boon can instead use their death-stealing
  gaze at will, regardless of their total number of growth
  points.</p><h3>Sentinel</h3><p><b>Source </b><a><i>Book of the Damned pg.
  102</i></a><br /><b>1: Gargoyle Magic (Sp)</b> <i>stone fist </i> 3/day,
  <i>protection from arrows</i> 2/day, or <i>fly</i> 1/day<br /><b>2: Idol of
  Desolation (Sp)</b> You can take on the strength of stone without sacrificing
  your awareness to temporarily become a sentinel of stone. You can cast
  <i>statue</i> as a spell-like ability once per day.<br /><b>3: Sculptor's
  Strike (Su)</b> The strike of your weapons causes a specified foe to calcify
  and harden into stone, bit by bit, eventually transforming that enemy into a
  favored child of Xoveron. To use sculptor's strike, you must first designate a
  foe in sight within 30 feet as a standard action. Once you've designated the
  foe, whenever you deal damage to that creature with a melee weapon (including
  unarmed strikes and natural weapons), the creature takes 1d6 points of
  Dexterity drain in addition to the normal damage. If the target succeeds at a
  Fortitude save (DC = 10 + half your Hit Dice + your Charisma modifier) this
  drain is reduced to 1 point. A creature drained to 0 Dexterity in this way is
  permanently transformed into a gargoyle (as per <i>polymorph any object</i>,
  except no Fortitude save is allowed) and must succeed at a Will save (using
  this ability's DC) or have its mind also become that of a gargoyle (as per
  <i>baleful polymorph</i>). When a creature is transformed in this way, all
  Dexterity drain caused by this effect is instantly healed and the creature's
  hit points are fully restored. The mental change can be reversed by any effect
  that removes curses, but the physical transformation can only be reversed by
  <i>miracle</i> or <i>wish</i>. You can activate sculptor's strike once per day
  to designate a foe, but once you've done so, that foe remains susceptible to
  your ability until you or it is slain. If you designate a different foe on a
  later day, any previously designated foe is no longer designated and can no
  longer be affected by this ability (unless you designate it once again at a
  later date).</p><h2>For Followers of Xoveron</h2><h3>Feats</h3><p>Channel
  Discord</p><h3>Magic Items - Wondrous Items</h3><p>Dagon's
  Eye</p><h3>Traits</h3><p>Demonic Persuasion</p><div><div><iframe>
  </iframe></div>
   </div>
name: Xoveron
pages: []

