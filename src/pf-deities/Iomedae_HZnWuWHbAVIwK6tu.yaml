_id: HZnWuWHbAVIwK6tu
_key: '!journal!HZnWuWHbAVIwK6tu'
content: >-
  <h1>Iomedae</h1><h2>The Inheritor</h2><b>Source</b> <a><i>Inner Sea Gods pg.
  76</i></a><br /><b>Pathfinder Wiki</b> <a
  href="http://www.pathfinderwiki.com/wiki/Iomedae">Iomedae</a><h3>Details</h3><p><b>Alignment</b>
  LG<br /><b>Pantheon</b> Core Deities<br /><b>Other Pantheons</b> Ascended
  Pantheon, Halfling Deities, Order of the God Claw Pantheon<br /><b>Areas of
  Concern</b> Honor, justice, rulership, valor<br /><b>Domains</b> Glory, Good,
  Law, Sun, War<br /><b>Subdomains</b> Archon (Good), Archon (Law), Chivalry,
  Day, Duels, Heroism, Honor, Hubris (Glory)*, Light, Redemption, Revelation,
  Soverignty, Tactics<br /><i>* Requires the <u>Acolyte of Apocrypha</u>
  trait.</i><br /><b>Favored Weapon</b> Longsword<br /><b>Symbol</b> Sword and
  sun<br /><b>Sacred Animal(s)</b> Lion<br /><b>Sacred Color(s)</b> Red,
  white<br /></p><h3>Obedience</h3><p>Hold your primary weapon in front of you
  and hang a holy symbol of Iomedae from it. Kneel while focusing on the holy
  symbol, pray for guidance and protection from the Inheritor, and swear to
  follow her teachings. Gain a +4 sacred bonus on Diplomacy and Knowledge
  (nobility) checks.</p><h3>Divine Gift</h3><p><b>Source</b> <a><i>Planar
  Adventures pg. 77</i></a><br /><i>Nethys Note: See here for details on how to
  gain a Divine Gift</i><br />By spending 10 minutes in prayer to Iomedae (this
  may take place during daily preparation of spells), a character can activate a
  <i>holy aura</i> or <i>shield of law</i> effect on himself (CL 20th) that
  lasts for the next 12 hours.</p><h3>On Golarion</h3><p><b>Centers of
  Worship</b> Absalom, Andoran, Cheliax, Galt, Lastwall, Mendev, Molthune,
  Nirmathas, Sargava<br /><b>Nationality</b> Chelaxian</p><h2>Boons - Deific
  Obedience</h2><h3> Evangelist</h3><p><strong>Source</strong>: Inner Sea Gods
  pg. 76<br /><b>1: Courageous (Sp)</b> <i>remove fear </i>3/day, <i>blessing of
  courage and life</i><sup>APG</sup> 2/day, or <i>heroism </i>1/day<br /><b>2:
  Demon-Feared Caster (Ex)</b> You are used to fighting the forces of evil. You
  gain a sacred bonus equal to 1 + 1 for every 4 Hit Dice you possess (maximum
  +6) on caster level checks to overcome the spell resistance of outsiders with
  the chaotic or evil subtypes. These bonuses stack against outsiders who are
  both chaotic and evil (maximum +12) . If you don't have the ability to cast
  spells, you instead gain the ability to use <i>protection from chaos/evil
  </i>as a spell-like ability three times per day.<br /><b>3: Wrath of the
  Inheritor (Su)</b> Three times per day, you can call upon Iomedae during the
  casting of a spell to increase its potency. When you use this ability, you can
  cast any spell that deals hit point damage and has a casting time of 1
  standard action as a full-round action instead. Doing so changes half the
  damage dealt to divine power, similar to a <i>flame strike </i>spell. For
  example, a wizard 5/evangelist 9 casts a <i>lightning bolt </i>as a full-round
  action. The spell deals 10d6 points of damage, half of which is electricity
  damage and the other half of which is divine energy and not subject to
  electricity resistance. If you can't cast spells that deal hit point damage,
  you instead gain the ability to imbue your weapon with holy power. Three times
  per day as a free action, you can grant your weapon the <i>holy</i> weapon
  special ability for 1 minute.</p><h3>Exalted</h3><p><strong>Source</strong>:
  Inner Sea Gods pg. 76<br /><b>1: Glorious Servant (Sp)</b> <i>shield of faith
  </i>3/day, <i>enthrall </i>2/day, or <i>searing light </i>1/day<br /><b>2:
  Righteous Strike (Sp)</b> Once per day, you can channel the effects of <i>holy
  smite </i>through your weapon. You must declare your use of this ability
  before you roll your attack. On a hit, the target is affected as if targeted
  with <i>holy smite</i>.<br /><b>3: Just Ally (Sp)</b> Once per day as a
  standard action, you can summon a shield archon (<i>Pathfinder RPG Bestiary 2
  </i>31). The shield archon follows your commands perfectly for 1 minute for
  every Hit Die you possess before vanishing back to its home in Heaven. The
  shield archon doesn't follow commands that would violate its alignment,
  however, and particularly egregious commands could cause it to attack
  you.</p><h3>Sentinel</h3><p><strong>Source</strong>: Inner Sea Gods pg. 76<br
  /><b>1: Knight of Valor (Sp)</b> <i>bless weapon </i>3/day, <i>bull's strength
  </i>2/day, or <i>magic vestment </i>1/day<br /><b>2: Valorous Smite (Su)</b>
  If you have the smite evil class feature, you gain an extra use of that
  ability per day. You add the levels of sentinel to your paladin levels when
  calculating the extra damage dealt by your smite. If you successfully deal
  damage with your smite, your target must succeed at a Will saving throw (with
  a DC equal to 10 + your Charisma modifier + 1/2 your Hit Dice) or be stunned
  for 1 round plus 1 round for every 4 Hit Dice you possess (maximum 6 rounds).
  Once a target saves against this stunning effect, it is immune to the stunning
  effect from your holy smite for 24 hours.</p><p>If you don't have the smite
  evil class feature, instead you can, as a free action, single out an outsider
  with the evil subtype or an evil-aligned dragon you plan to vanquish. Against
  this target, you gain a +2 sacred bonus on attack rolls and a sacred bonus
  equal to your sentinel level on damage rolls. The bonuses remain until the
  target is dead or you use this ability again, whichever comes first. If you
  choose a target that is not one of the listed creature types, the ability is
  wasted. You can use this ability a number of times per day equal to your
  Charisma bonus (minimum once per day).<br /><b>3: Banishing Strike (Sp)</b>
  Once per day, you can channel the effects of <i>banishment </i>through your
  weapon, though you don't need to cast (or even know) the spell. You must
  declare your use of this ability before you roll your attack. On a hit, the
  target is affected by a <i>banishment </i>effect. If you openly wear a holy
  symbol of Iomedae, you gain a +1 bonus on your caster level check to overcome
  the target's spell resistance (if any) and the saving throw DC increases by
  2.</p><h2>Paladin Code</h2>The paladins of Iomedae are just and strong,
  crusaders who live for the joy of righteous battle. Their mission is to right
  wrongs and eliminate evil at its root. They serve as examples to others, and
  their code demands they protect the weak and innocent by eliminating sources
  of oppression, rather than merely the symptoms. They may back down or withdraw
  from a fight if they are overmatched, but if their lives will buy time for
  others to escape, they must give them. Their tenets include the following
  affirmations. <ul><li>I will learn the weight of my sword. Without my heart to
  guide it, it is worthless-my strength is not in my sword, but in my heart. If
  I lose my sword, I have lost a tool. If I betray my heart, I have
  died.</li><li>I will have faith in the Inheritor. I will channel her strength
  through my body. I will shine in her legion, and I will not tarnish her glory
  through base actions.</li><li>I am the first into battle, and the last to
  leave it.</li><li>I will not be taken prisoner by my free will. I will not
  surrender those under my command.</li><li>I will never abandon a companion,
  though I will honor sacrifice freely given.</li><li>I will guard the honor of
  my fellows, both in thought and deed, and I will have faith in
  them.</li><li>When in doubt, I may force my enemies to surrender, but I am
  responsible for their lives.</li><li>I will never refuse a challenge from an
  equal. I will give honor to worthy enemies, and contempt to the rest.
  </li><li>I will suffer death before dishonor.</li><li>I will be temperate in
  my actions and moderate in my behavior. I will strive to emulate Iomedae's
  perfection.</li></ul><h2>Divine Fighting Technique</h2><h3>Iomedae's Inspiring
  Sword</h3><p><b>Source</b> <a><i>Weapon Master's Handbook pg. 11</i></a><br
  />Iomedae demonstrates how to turn a longsword into a shining beacon of
  hope.</p><p><b>Optional Replacement</b>: A paladin who worships Iomedae can
  replace a mercy with this initial benefit, even if she doesn't meets its
  prerequisites.</p><p><b>Initial Benefit</b>: While wielding a longsword, you
  can perform an impressive display of prowess as a full-round action. All
  allies within 30 feet who can see your display gain a +2 sacred bonus on
  attack rolls, saving throws, and skill checks for 1 round + 1 round per 5
  points of base attack bonus you have.</p><p><b>Advanced Prerequisites</b>:
  Dazzling Display, Weapon Focus (longsword), base attack bonus
  +10.</p><p><b>Optional Replacement</b>: A paladin of at least 9th level who
  worships Iomedae can replace a mercy with the following advanced benefit, even
  if she doesn't meet the benefit's prerequisites.</p><p><b>Advanced
  Benefit</b>: As a standard action or at the end of a charge, you can make an
  attack against a foe with a longsword. If you succeed, you grant all allies
  who can see your attack a +2 sacred bonus on attack rolls, saving throws, and
  skill checks for 1 minute.</p><h2>For Followers of
  Iomedae</h2><h3>Archetypes</h3><p>Battle Scout (Ranger), Crusader (Cleric),
  Iomedaen Enforcer (Paladin), Shining Knight (Paladin), Standard Bearer
  (Cavalier)</p><h3>Feats</h3><p>Disciple of the Sword, Hands of Valor,
  Protective Channel, Strike True</p><h3>Magic Items - Altars</h3><p>Altar of
  Iomedae</p><h3>Magic Items - Armor</h3><p>Icon of Order, Inheritor's
  Breastplate, War Commander's Field Plate</p><h3>Magic Items -
  Rings</h3><p>Knight-Inheritor's Ring</p><h3>Magic Items -
  Weapons</h3><p>Crusader's Longsword, Inheritor's Light, Iomedae's
  Needle</p><h3>Magic Items - Wondrous Items</h3><p>Knight's Pennon (Battle),
  Cloak of the Crusader, Lord's Banner (Crusades), Glorious Tabard (Greater),
  Knight's Pennon (Honor), Inheritor's Gauntlet, Glorious Tabard (Lesser), Medal
  of the Hero's Heart, Knight's Pennon (Parley), Lord's Banner (Swiftness),
  Lord's Banner (Terror), Lord's Banner (Victory)</p><h3>Monsters</h3><p>Hand of
  the Inheritor (Herald), Iophanite</p><h3>Prestige Classes</h3><p>Heritor
  Knight, Inheritor's Crusader, Knight of Ozem</p><h3>Spells</h3><p>Bestow Grace
  of the Champion, Burst of Glory, Inheritor's Smite, Light Prison, Litany of
  Righteousness, Spear of Purity, Weapons Against Evil</p><h3>Traits</h3><p>A
  Shining Beacon, Divine Warrior, Inheritor's Immunity, Purity of Faith, Regal
  Presence, Searing Beacon</p><h2>Unique Spell Rules</h2><b>Source
  </b><a><i>Inner Sea Gods pg. 83</i></a><br
  /><h3>Cleric/Warpriest</h3><p><i>Holy Sword</i> can be prepared as a 8th-level
  spell<br /><i>Good Hope</i> can be prepared as a 4th-level spell<br /><i>Mark
  of Justice</i> can be prepared as a 4th-level spell<br
  /></p><h3>Inquisitor</h3><p><i>Holy Sword</i> can be prepared as a 6th-level
  spell<br /></p><h3>Paladin</h3><p><i>Good Hope</i> can be prepared as a
  3rd-level spell<br /><i>Mark of Justice</i> can be prepared as a 3rd-level
  spell</p><h2>Unique Summon Rules</h2><b>Source </b><a><i>Pathfinder #26: The
  Sixfold Trial pg. 71</i></a><br /><b>Summon Monster IV</b>: Celestial Lion -
  LG<br /><b>Summon Monster VI</b>: Celestial Griffon - NG<br /><b>Summon
  Nature's Ally VI</b>: Celestial Griffon - NG</p><p><div><div><iframe>
  </iframe></div>
   </div>
name: Iomedae
pages: []

