_id: k5QEuyJYIcrEX8rj
_key: '!journal!k5QEuyJYIcrEX8rj'
content: >-
  <h1>Erastil</h1><h2>Old Deadeye</h2><b>Source</b> <a><i>Inner Sea Gods pg.
  52</i></a><br /><b>Pathfinder Wiki</b> <a
  href="http://www.pathfinderwiki.com/wiki/Erastil">Erastil</a><h3>Details</h3><p><b>Alignment</b>
  LG<br /><b>Pantheon</b> Core Deities<br /><b>Other Pantheons</b> Gnomish
  Deities, Halfling Deities, Sandpoint Pantheon<br /><b>Areas of Concern</b>
  Family, farming, hunting, trade<br /><b>Domains</b> Animal, Community, Good,
  Law, Plant<br /><b>Subdomains</b> Archon (Good), Archon (Law), Education
  (Community), Family, Feather, Fur, Growth, Home<br /><i>* Requires the
  <u>Acolyte of Apocrypha</u> trait.</i><br /><b>Favored Weapon</b> Longbow<br
  /><b>Symbol</b> Bow and arrow<br /><b>Sacred Animal(s)</b> Stag<br /><b>Sacred
  Color(s)</b> Brown, green<br /></p><h3>Obedience</h3><p>Plant five seeds in
  fertile earth, spacing them out in the shape of an arrow. The seeds may be
  those of any plant that can potentially grow in the region, though edible
  plants are preferred. The seeds don't need to be from a type of plant that
  could thrive in that soil-all they must have is a chance at survival. If no
  suitable earth exists, place a small bundle of seeds (again, those that grow
  edible plants are preferred), a small bundle of preserved food, or a quiver of
  arrows in a place where a passerby might see it. Mark your gifts with
  Erastil's sign, and say a prayer for the health and safety of the communities
  in the area and those who may be in need of Erastil's guidance. Gain a +4
  sacred bonus on Survival checks.</p><h3>Divine Gift</h3><p><b>Source</b>
  <a><i>Planar Adventures pg. 76</i></a><br /><i>Nethys Note: See here for
  details on how to gain a Divine Gift</i><br />The character gains three
  <i>greater slaying arrows</i>. The designated creatures for each of these
  three arrows is determined by the GM. The magic of these <i>greater slaying
  arrows</i> vanishes if they are sold, but they can be given to others in trade
  for goods or as a gift. If a worshiper of Erastil fires one of these
  <i>greater slaying arrows</i> and misses, the arrow vanishes and a <i>slaying
  arrow</i> of the same type appears in the gifted character's quiver. If this
  regular <i>slaying arrow</i> is then fired, only to miss its target, it has a
  50% chance to be lost, as normal.</p><h3>On Golarion</h3><p><b>Centers of
  Worship</b> Andoran, Cheliax, Galt, Isger, Lands of the Linnorm Kings, Mana
  Wastes, Molthune, Nirmathas, River Kingdoms, Varisia<br /><b>Nationality</b>
  Ulfen</p><h2>Boons - Deific Obedience</h2><h3> Evangelist</h3><p><b>Source
  </b><a><i>Inner Sea Gods pg. 52</i></a><br /><b>1: Family's Bond (Sp)</b>
  <i>cure light wounds </i>3/day, <i>shield other </i>2/day, or <i>prayer
  </i>1/day<br /><b>2: Twin Fang (Ex)</b> Once per day as a standard action, you
  can summon an exact double of your animal companion. The double acts and
  thinks like your animal companion in every way, and obeys your commands just
  as the original would. Your original animal companion and its double
  understand and trust each other perfectly. The double remains for 1 round for
  every Hit Die you possess, and then vanishes. If you don't have an animal
  companion, you instead gain the ability to use <i>summon nature's ally V</i>
  as a spell-like ability once per day.<br /><b>3: Faithful Archer (Ex)</b> You
  are particularly skilled at using Erastil's favored weapon. When using a
  longbow, you add your Wisdom bonus on attack and damage rolls against targets
  within 30 feet.</p><h3>Exalted</h3><p><strong>Source</strong>: Inner Sea Gods
  pg. 52<br /><b>1: Animal Friend (Sp)</b> <i>charm animal </i>3/day, <i>animal
  messenger </i>2/day, or <i>summon nature's ally III </i>1/day<br /><b>2:
  Hunter's Ally (Sp)</b> Once per day as a standard action, you can summon a
  pair of hound archons. The hound archons follow your commands perfectly for 1
  minute for every Hit Die you possess before vanishing back to their home in
  Heaven. The hound archons don't follow commands that would violate their
  lawful good alignment. Such commands will not only earn refusal and scorn from
  the hound archons, but could cause them to attack you if the command is
  particularly egregious.<br /><b>3: Communal Table (Sp)</b> Your devotion to
  caring for your community allows you to evoke Erastil's divine bounty to feed
  your friends with a banquet of cooked game and simple beverages. Once per day,
  you can use heroes' feast as a spell-like ability. Creatures that eat from
  this communal table, a process that takes 1 hour, gain a +2 sacred bonus on
  attack rolls and Will saving throws instead of the usual +1 morale bonus.
  Whenever you cast this spell, choose one teamwork feat; you may select a new
  feat every time you cast this spell, but once it's cast, your selection can't
  be changed. Anyone who eats from the communal table gains the benefits of the
  chosen teamwork feat. The benefits of the heroes' feast, including the bonus
  teamwork feat, last for 12 hours. A character doesn't need to meet the
  prerequisites for a teamwork feat granted through this
  ability.</p><h3>Sentinel</h3><p><strong>Source</strong>: Inner Sea Gods pg.
  52<br /><b>1: Sureshot (Sp)</b> <i>longshot</i><sup>UC</sup> 3/day,
  <i>deadeye's arrow </i>2/day, or <i>flame arrow </i>1/day<br /><b>2: Tough
  Hide (Su)</b> When you wear armor made from leather or animal hide, the armor
  provides an extra +2 armor bonus to your AC. This bonus improves the normal
  armor bonus granted by the armor; in other words, it stacks with the suit's
  normal armor bonus. You also mystically subsume some of the qualities of the
  animal that gave its life for the armor. Gain the scent ability with a range
  of 30 feet while wearing armor made from leather or animal hide.<br /><b>3:
  Farmer's Bond (Su)</b> Your time spent cultivating crops has given you a bond
  with plants. You can cast <i>speak with plants </i>3 times per day. If you
  spend at least 1 hour during the day in direct sunlight, you don't need to eat
  that day. Finally, you become immune to poisons ingested from whole plants
  (not distilled poisons, such as assassins use) and any poison from attacks or
  effects generated by plant creatures.</p><h2>Paladin Code</h2>The paladins of
  Erastil are gruff, strict traditionalists. They seek to preserve the integrity
  of rural life and communities. Their tenets include the following
  affirmations. <ul><li>My community comes first, and I will contribute to it
  all that I can. If I don't give something back, who will?</li><li>I must offer
  the poor in my community assistance, but I may not do the work for
  them-instead, I must teach them to contribute to the settlement. It is only
  through cooperation that a community grows strong.</li><li>When danger
  threatens, I am not a fool. I seek first to make sure the weak and innocent
  are safe, and then I quell the danger.</li><li>I keep to the old ways, the
  true ways. I am not seduced by the lure of money or power. I remember that
  true honor comes from within, not from the accolades of others.</li><li>I
  remember that reputation is everything. Mine is pure and upstanding, and I
  will repair it if it is broken or tarnished. I stand by my decisions, and live
  so that none shall have cause to blame me.</li><li>I show respect to my
  elders, for they have done much. I show respect to the young, for they have
  much left to do. I show respect to my peers, for they carry the load. And I
  shall carry it with them.</li><li>I am honest, trustworthy, and stable. If I
  must leave my lands and community, before I go, I ensure that they will be
  tended in my absence. Even when duty calls, my duties to my home come
  first-letting them lapse makes me a burden on my people.</li></ul><h2>Divine
  Fighting Technique</h2><h3>Erastil's Distracting Shot</h3><p><b>Source</b>
  <a><i>Divine Anthology pg. 29</i></a><br />Sometimes used as a companion piece
  to the <i>Parables of Erastil</i>, the lesser-known <i>Horns of the Elk</i> is
  a manual completely dedicated to archery. Lacking the folktales that the
  <i>Parables</i> are celebrated for, the <i>Horns of the Elk</i> instead
  focuses on covering a wide array of beginner and advanced archery tactics. A
  copy of the manual can be found virtually anywhere a shrine to Erastil
  exists.</p><p><b>Optional Replacement</b>: A lawful good ranger who worships
  Erastil can choose Divine Fighting Technique as the bonus feat granted by his
  combat style if he chooses archery as his combat style.<br /><br /><b>Initial
  Benefit</b>: As a standard action, you can fire a distracting shot from your
  longbow or shortbow; when you do, select one ally who is adjacent to the
  creature you are targeting with the distracting shot. If you hit the creature,
  the chosen ally gains a +2 bonus to her Armor Class against that creature.
  This bonus to her Armor Class lasts until the start of your next
  turn.</p><p><b>Advanced Prerequisites</b>: Dex 17, Divine Fighting Technique,
  Point-Blank Shot, Precise Shot, base attack bonus +10.</p><p><b>Optional
  Replacement</b>: A lawful good ranger who worships Erastil and has the Divine
  Fighting Technique feat can replace the bonus feat granted by his combat style
  at 10th level with the following advanced benefit without meeting its
  prerequisites.</p><p><b>Advanced Benefit</b>: When you fire a distracting shot
  at a creature and hit the creature, you grant a +4 Armor Class bonus to the
  selected adjacent ally and a +2 Armor Class bonus to all other allies within
  30 feet of the selected adjacent ally against attacks by the targeted
  creature.</p><h2>For Followers of Erastil</h2><h3>Archetypes</h3><p>Archer
  (Fighter), Divine Hunter (Paladin), Sniper (Rogue), Trapper
  (Ranger)</p><h3>Feats</h3><p>Erastil's Blessing, Nimble Natural Summons,
  Savior's Arrow, Thicket Channel</p><h3>Magic Items - Altars</h3><p>Altar of
  Erastil</p><h3>Magic Items - Armor</h3><p>Deadeye Leather</p><h3>Magic Items -
  Rings</h3><p>Deadeye's Spotter Ring</p><h3>Magic Items -
  Weapons</h3><p>Kinbonded Bow</p><h3>Magic Items - Wondrous
  Items</h3><p>Bracers of Archery (Greater), Bracers of Archery (Lesser), Bronze
  Skinning Knife, Cloak of Elvenkind, Efficient
  Quiver</p><h3>Monsters</h3><p>Stag Archon, The Grim White Stag
  (Herald)</p><h3>Prestige
  Classes</h3><p>Hinterlander</p><h3>Spells</h3><p>Allfood, Deadeye's Arrow,
  Deadeye's Lore, Hunter's Blessing, Tracking Mark</p><h3>Traits</h3><p>Deadeye
  Bowman, Erastil's Speaker, Patient Optimist, Provider, Wise
  Teacher</p><h2>Unique Spell Rules</h2><b>Source </b><a><i>Inner Sea Gods pg.
  59</i></a><br /><h3>Cleric/Warpriest</h3><p><i>Goodberry</i> can be prepared
  as a 2nd-level spell<br /><i>Animal Messenger</i> can be prepared as a
  2nd-level spell [may use on any nonhostile animal (including friendly guard
  animals and domesticated animals)]<br /></p><h3>Druid</h3><p><i>Goodberry</i>
  can be prepared as a 1st-level spell [can cast on nuts or seeds as well as
  berries]<br /></p><h3>Paladin</h3><p><i>Goodberry</i> can be prepared as a
  2nd-level spell<br /><i>Animal Messenger</i> can be prepared as a 2nd-level
  spell [may use on any nonhostile animal (including friendly guard animals and
  domesticated animals)]<br /></p><h3>Ranger</h3><p><i>Goodberry</i> can be
  prepared as a 2nd-level spell</p><h2>Unique Summon Rules</h2><b>Source
  </b><a><i>Pathfinder #32: Rivers Run Red pg. 71</i></a><br /><b>Summon Monster
  II</b>: Celestial Elk - LG<br /><b>Summon Monster III</b>: Celestial Dire Boar
  - LG<br /><h2>Other Rules</h2><b>Source </b><a><i>Inner Sea Gods pg.
  55</i></a><br />Clerics and druids of Erastil are proficient with the
  shortbow</p><p><div><div><iframe> </iframe></div>
   </div>
name: Erastil
pages: []

