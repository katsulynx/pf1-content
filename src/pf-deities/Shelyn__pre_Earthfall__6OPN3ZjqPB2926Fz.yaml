_id: 6OPN3ZjqPB2926Fz
_key: '!journal!6OPN3ZjqPB2926Fz'
content: >-
  <h1>Shelyn</h1><h2>The Eternal Rose</h2><b>Source</b> <a><i>Inner Sea Gods pg.
  140</i></a><br /><b>Pathfinder Wiki</b> <a
  href="http://www.pathfinderwiki.com/wiki/Shelyn">Shelyn</a><h3>Details</h3><p><b>Alignment</b>
  NG<br /><b>Pantheon</b> Core Deities<br /><b>Other Pantheons</b> Deities of
  Tian Xia, Gnomish Deities, Halfling Deities, Sandpoint Pantheon, Taldan
  Pantheon<br /><b>Areas of Concern</b> Art, beauty, love, music<br
  /><b>Domains</b> Air, Charm, Good, Luck, Protection<br /><b>Subdomains</b>
  Agathion, Captivation*, Cloud, Defense, Fate, Love, Purity<br /><i>* Requires
  the <u>Acolyte of Apocrypha</u> trait.</i><br /><b>Favored Weapon</b>
  Glaive<br /><b>Symbol</b> Multicolored songbird<br /><b>Sacred Animal(s)</b>
  Songbird<br /><b>Sacred Color(s)</b> All<br /></p><h3>Obedience</h3><p>Paint a
  small picture, compose a short poem or song, dance a scene from a ballet, or
  create another work of art, whispering praise to Shelyn's beauty and grace as
  you do so. The art piece need be neither large nor complex, but heartfelt and
  made to the best of your ability. Gift the piece of art to a stranger and pay
  her a sincere compliment as you do so. If there are no suitable individuals
  around to receive the gift, leave it in an obvious place with a note praising
  Shelyn and asking whoever finds it to take it with your warmest wishes. Gain a
  +4 sacred bonus on Craft and Perform checks.</p><h3>Divine
  Gift</h3><p><b>Source</b> <a><i>Planar Adventures pg. 80</i></a><br
  /><i>Nethys Note: See here for details on how to gain a Divine Gift</i><br
  />Shelyn grants a permanent +1 untyped bonus to Charisma.</p><h3>On
  Golarion</h3><p><b>Centers of Worship</b> Absalom, Galt, Sargava, Taldor<br
  /><b>Nationality</b> Taldan</p><h2>Boons - Deific Obedience</h2><h3>
  Evangelist</h3><p><strong>Source</strong>: Inner Sea Gods pg. 140<br /><b>1:
  Fiction Weaver (Sp)</b> <i>silent image </i>3/day, <i>minor image </i>2/day,
  or <i>major image </i>1/day<br /><b>2: Versatile Artist (Ex)</b> Drawing on
  Shelyn's divine inspiration, you can substitute your bonus in a specific
  Perform skill for your bonus in another related skill, as the versatile
  performance class feature. Select one type of Perform skill which you can
  substitute for its associated skills. If you already have the versatile
  performance class feature when you obtain this boon, choose an additional
  Perform skill to substitute; you gain a +2 bonus on checks with this
  additional Perform skill.<br /><b>3: Persuasive Aesthetic (Sp)</b> You learned
  to pour your soul into works of art. Once per week, you can use <i>symbol of
  persuasion </i>as a spell-like ability. Instead of inscribing a symbol, you
  can cast this spell using a piece of art worth 5,000 gp or more as an arcane
  focus. Unlike the material components of <i>symbol of persuasion</i>, the
  artwork is not consumed during casting. Each creature within 60 feet of the
  artwork must succeed at a Will saving throw or be charmed by you (as the
  <i>charm monster </i>spell) for 1 hour for every Hit Die you possess. Covering
  or hiding the artwork renders its persuasive effect inactive until it is
  uncovered again. The persuasive effect lasts until the symbol is triggered or
  until you use this ability again, whichever comes first. You may choose a
  different piece of art every time you use this
  ability.</p><h3>Exalted</h3><p><strong>Source</strong>: Inner Sea Gods pg.
  140<br /><b>1: Devotionals (Sp)</b> <i>unbreakable heart </i>3/day, <i>calm
  emotions </i>2/day, or <i>good hope </i>1/day<br /><b>2: Joyous Ally (Sp)</b>
  Your sense of beauty and the loyalty you bear your goddess have attracted the
  notice of her celestial servants. Once per day as a standard action, you can
  summon a lillend azata from Shelyn's divine realm in Nirvana to aid you. You
  gain telepathy with the lillend to a range of 100 feet. The lillend follows
  your commands perfectly for 1 minute for every Hit Die you possess before
  vanishing back to its home on Elysium. The lillend doesn't follow any commands
  that would cause it to commit evil acts or destroy works of art, and the
  creature could even attack you if the command is particularly egregious.<br
  /><b>3: Plumed Blade (Su)</b> Even in battle, you partake of the beauty and
  joy with which Shelyn graces her devoted followers. As a free action, you can
  cause an illusion of brightly colored feathers to follow every swipe and
  motion of your weapon. When you do so, a single weapon you hold gains the
  <i>holy </i>and <i>shock </i>weapon special abilities. (You can use this
  ability on a ranged weapon, but can't apply it directly to a piece of
  ammunition.) If you drop the weapon or give it away, this ability's effects
  immediately end. You can grant weapons this ability for a number of rounds per
  day equal to 1 + 1 for every 4 Hit Dice you possess (maximum 6 rounds). The
  rounds don't need to be
  consecutive.</p><h3>Sentinel</h3><p><strong>Source</strong>: Inner Sea Gods
  pg. 140<br /><b>1: Graceful Warrior (Sp)</b> <i>animate rope </i>3/day,
  <i>cat's grace </i>2/ day, or <i>haste </i>1/day<br /><b>2: Glorious Might
  (Su)</b> Your passionate devotion to Shelyn grants you extra prowess in your
  battles against evil, allowing you to see and root out the ugliness at the
  heart of your foe. When you use your smite evil class feature, you gain double
  your Charisma bonus on attack rolls and your Charisma bonus + your paladin
  level on damage rolls. This replaces the normal bonuses for using smite evil.
  If you don't have access to smite evil, you instead gain a +2 bonus on weapon
  attack rolls against evil targets.<br /><b>3: Cloak of Feathers (Su)</b> An
  aura of colorful features, glowing with Shelyn's holy grace, shields you
  against electricity attacks with a measure of the protection enjoyed by the
  azatas who serve her. You gain electricity resistance 15.</p><h2>Paladin
  Code</h2>The paladins of Shelyn are peaceable promoters of art and beauty.
  They see the ugliness in evil, even when cloaked in the form of beauty, and
  their mission is to defend those who devote their lives to the creation of
  beauty, bring it forth themselves, and prevent the weak and foolish from being
  seduced by false promises. Their tenets include the following adages.
  <ul><li>I see beauty in others. As a rough stone hides a diamond, a drab face
  may hide the heart of a saint.</li><li>I am peaceful. I come first with a rose
  rather than a weapon, and act to prevent conflict before it blossoms. I never
  strike first, unless it is the only way to protect the innocent.</li><li>I
  accept surrender if my opponent can be redeemed-and I never assume that they
  cannot be. All things that live love beauty, and I will show beauty's answer
  to them.</li><li>I live my life as art. I will choose an art and perfect it.
  When I have mastered it, I will choose another. The works I leave behind make
  life richer for those who follow.</li><li>I will never destroy a work of art,
  nor allow one to come to harm, unless greater art arises from its loss. I will
  only sacrifice art if doing so allows me to save a life, for untold beauty can
  arise from an awakened soul.</li><li>I lead by example, not with my blade.
  Where my blade passes, a life is cut short, and the world's potential for
  beauty is lessened.</li></ul><h2>For Followers of
  Shelyn</h2><h3>Archetypes</h3><p>Sin Eater (Inquisitor), Songhealer
  (Bard)</p><h3>Feats</h3><p>Bladed Brush, Clarifying Channel, Divine
  Expression, Persuasive Performer</p><h3>Magic Items - Altars</h3><p>Altar of
  Shelyn</p><h3>Magic Items - Armor</h3><p>Rosy Hauberk</p><h3>Magic Items -
  Rings</h3><p>Ring of Seven Lovely Colors</p><h3>Magic Items -
  Weapons</h3><p>Blade of Three Fancies</p><h3>Magic Items - Wondrous
  Items</h3><p>Beautiful War Paint, Boots of the Eternal Rose, Bracelet of
  Friends, Harp of Charming, Marvelous Pigments, Perfect Tuning Fork, Zonzon
  Doll of Forgiveness</p><h3>Monsters</h3><p>Dapsara, The Spirit of Adoration
  (Herald)</p><h3>Prestige Classes</h3><p>Devoted
  Muse</p><h3>Spells</h3><p>Adoration, Aspect of the Nightingale, Joyful
  Rapture, Tap Inner Beauty, Trail of the Rose, Unbreakable Heart, Waves of
  Ecstasy</p><h3>Traits</h3><p>Ear for Music, Inner Beauty, Intense Artist,
  Self-Sacrifice, Unswaying Love</p><h2>Unique Spell Rules</h2><b>Source
  </b><a><i>Inner Sea Gods pg. 147</i></a><br
  /><h3>Cleric/Warpriest</h3><p><i>Charm Person</i> can be prepared as a
  1st-level spell<br /><i>Good Hope</i> can be prepared as a 4th-level spell<br
  /><i>Sympathy</i> can be prepared as a 8th-level spell [may only cast on works
  of art]<br /><i>Charm Animal</i> can be prepared as a 2nd-level spell [may be
  prepared by any priest of Shelyn, not just clerics/paladins]<br
  /></p><h3>Inquisitor</h3><p><i>Charm Person</i> can be prepared as a 1st-level
  spell<br /><i>Good Hope</i> can be prepared as a 4th-level spell<br
  /></p><h3>Paladin</h3><p><i>Charm Person</i> can be prepared as a 1st-level
  spell<br /><i>Good Hope</i> can be prepared as a 4th-level spell<br /><i>Charm
  Animal</i> can be prepared as a 2nd-level spell [may be prepared by any priest
  of Shelyn, not just clerics/paladins]<br /></p><h2>Unique Summon
  Rules</h2><b>Source </b><a><i>Pathfinder #50: Night of Frozen Shadows pg.
  75</i></a><br /><b>Summon Monster II</b>: Grig (extraplanar)<br /><b>Summon
  Monster III</b>: Silvanshee<br /><b>Summon Monster V</b>: Vulpinal<br /><br
  /><div><div><iframe> </iframe></div>
   </div>
name: Shelyn (pre-Earthfall)
pages: []

