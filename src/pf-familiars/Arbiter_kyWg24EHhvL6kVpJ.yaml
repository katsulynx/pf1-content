_id: kyWg24EHhvL6kVpJ
_key: '!actors!kyWg24EHhvL6kVpJ'
img: icons/svg/mystery-man.svg
items:
  - _id: cV7yHt8i5YCV0ZTd
    _key: '!actors.items!kyWg24EHhvL6kVpJ.cV7yHt8i5YCV0ZTd'
    img: systems/pf1/icons/skills/yellow_40.jpg
    name: Outsider (Extraplanar, Inevitable, Lawful)
    system:
      bab: high
      classSkills:
        blf: true
        crf: true
        kpl: true
        per: true
        sen: true
        ste: true
      description:
        value: >-
          <p>An outsider is at least partially composed of the essence (but not
          necessarily the material) of some plane other than the Material Plane.
          Some creatures start out as some other type and become outsiders when
          they attain a higher (or lower) state of spiritual
          existence.</p><h2>Features</h2><p>An outsider has the following
          features.</p><ul><li>d10 Hit Dice.</li><li>Base attack bonus equal to
          total Hit Dice (fast progression).</li><li>Two good saving throws,
          usually&nbsp;<a
          href="https://www.d20pfsrd.com/gamemastering/combat#TOC-Reflex"
          rel="nofollow">Reflex</a>&nbsp;and Will.</li><li>Skill points equal to
          6 + Int modifier (minimum 1) per Hit Die. The following are class
          skills for outsiders: <a href="https://www.d20pfsrd.com/skills/bluff"
          rel="nofollow">Bluff</a>, <a
          href="https://www.d20pfsrd.com/skills/craft" rel="nofollow">Craft</a>,
          <a href="https://www.d20pfsrd.com/skills/knowledge"
          rel="nofollow">Knowledge</a>&nbsp;(planes), <a
          href="https://www.d20pfsrd.com/skills/perception"
          rel="nofollow">Perception</a>, <a
          href="https://www.d20pfsrd.com/skills/sense-motive"
          rel="nofollow">Sense Motive</a>, and <a
          href="https://www.d20pfsrd.com/skills/stealth"
          rel="nofollow">Stealth</a>. Due to their varied nature, outsiders also
          receive 4 additional class skills determined by the creature’s
          theme.</li></ul><h2>Traits</h2><p>An outsider possesses the following
          traits (unless otherwise noted in a creature’s
          entry).</p><ul><li>Darkvision 60 feet.</li><li>Unlike most living
          creatures, an outsider does not have a dual nature—its soul and body
          form one unit. When an outsider is slain, no soul is set loose. Spells
          that restore souls to their bodies, such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/raise-dead"
          rel="nofollow">raise dead</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/reincarnate"
          rel="nofollow">reincarnate</a>,&nbsp;and&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/r/resurrection"
          rel="nofollow">resurrection</a>,&nbsp;don’t work on an outsider. It
          takes a different magical effect, such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/l/limited-wish"
          rel="nofollow">limited wish</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/w/wish"
          rel="nofollow">wish</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/m/miracle"
          rel="nofollow">miracle</a>,&nbsp;or&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/t/true-resurrection"
          rel="nofollow">true resurrection</a>&nbsp;to restore it to life. An
          outsider with the native subtype can be raised, reincarnated, or
          resurrected just as other living creatures can be.</li><li>Proficient
          with all simple and martial weapons and any weapons mentioned in its
          entry.</li><li>Proficient with whatever type of armor (light, medium,
          or heavy) it is described as wearing, as well as all lighter types.
          Outsiders not indicated as wearing armor are not proficient with
          armor. Outsiders are proficient with shields if they are proficient
          with any form of armor.</li><li>Outsiders breathe, but do not need to
          eat or sleep (although they can do so if they wish). Native outsiders
          breathe, eat, and sleep.</li></ul>
      hd: 10
      hp: 11
      level: 2
      savingThrows:
        ref:
          value: high
        will:
          value: high
      skillsPerLevel: 6
      tag: outsider
    type: class
  - _id: vLH4zSRiPC7rl8l6
    _key: '!actors.items!kyWg24EHhvL6kVpJ.vLH4zSRiPC7rl8l6'
    img: systems/pf1/icons/skills/yellow_40.jpg
    name: 'Race: Outsider'
    system:
      creatureType: outsider
      description:
        value: >-
          sbc | As the statblock did not include a race, a custom one was
          generated.
      subTypes:
        - extraplanar
        - inevitable
        - lawful
    type: race
  - _id: lMkfTnDCVCB8WJCE
    _key: '!actors.items!kyWg24EHhvL6kVpJ.lMkfTnDCVCB8WJCE'
    img: systems/pf1/icons/skills/shadow_12.jpg
    name: 'Sense: Darkvision 60ft.'
    system:
      abilityType: ex
      description:
        value: >-
          Darkvision is the extraordinary ability to see with no light source at
          all, out to a range specified for the creature. Darkvision is
          black-and-white only (colors cannot be discerned). It does not allow
          characters to see anything that they could not see otherwise—invisible
          objects are still invisible, and illusions are still visible as what
          they seem to be. Likewise, darkvision subjects a creature to gaze
          attacks normally. The presence of light does not spoil darkvision.
      subType: racial
    type: feat
  - _id: NnwGXENLCsdAAq8h
    _key: '!actors.items!kyWg24EHhvL6kVpJ.NnwGXENLCsdAAq8h'
    img: systems/pf1/icons/skills/shadow_11.jpg
    name: 'Sense: Low-light Vision'
    system:
      abilityType: ex
      description:
        value: >-
          A creature with low-light vision can see twice as far as a human in
          starlight, moonlight, torchlight, and similar conditions of dim light.
          It retains the ability to distinguish color and detail under these
          conditions.
      subType: racial
    type: feat
  - _id: 63nvtPzCNOL2RDRs
    _key: '!actors.items!kyWg24EHhvL6kVpJ.63nvtPzCNOL2RDRs'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Flyby Attack
    system:
      description:
        value: >-
          sbc | As Flyby Attack could not be found in any compendium, a
          placeholder was generated.
    type: feat
  - _id: tw2HjKVs5qFqgFvB
    _key: '!actors.items!kyWg24EHhvL6kVpJ.tw2HjKVs5qFqgFvB'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Weapon Finesseb
    system:
      description:
        value: >-
          sbc | As Weapon FinesseB could not be found in any compendium, a
          placeholder was generated.
    type: feat
  - _id: ikAwLu6mv2lDEe0l
    _key: '!actors.items!kyWg24EHhvL6kVpJ.ikAwLu6mv2lDEe0l'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Special Quality: Locate Inevitable'
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: kAWfyaFgcOR3XvWo
    _key: '!actors.items!kyWg24EHhvL6kVpJ.kAWfyaFgcOR3XvWo'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Regeneration 2 (chaotic)
    system:
      description:
        value: sbc | Placeholder
      subType: misc
    type: feat
  - _id: pRyoriXIEMry3eB0
    _key: '!actors.items!kyWg24EHhvL6kVpJ.pRyoriXIEMry3eB0'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Constant Vigilance
    system:
      description:
        value: sbc | Placeholder
      subType: classFeat
    type: feat
  - _id: DEjBvfURyGMP407i
    _key: '!actors.items!kyWg24EHhvL6kVpJ.DEjBvfURyGMP407i'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Constructed
    system:
      description:
        value: sbc | Placeholder
      subType: classFeat
    type: feat
  - _id: f8oHt25vF6XiaSaj
    _key: '!actors.items!kyWg24EHhvL6kVpJ.f8oHt25vF6XiaSaj'
    img: icons/svg/mystery-man.svg
    name: Short Sword
    system:
      actions:
        - _id: iu5m9qkgza1irbi7
          ability:
            attack: str
            critRange: '19'
            damage: str
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          attackBonus: '3'
          attackName: Short Sword
          attackNotes:
            - short sword +7 (1d3/19-20)
          damage:
            parts:
              - formula: 1d3+0
                type:
                  custom: ''
                  values:
                    - piercing
          duration:
            units: inst
          img: icons/svg/mystery-man.svg
          name: Attack
          range:
            units: melee
          save:
            dc: 0
          tag: shortSword
      subType: weapon
    type: attack
  - _id: rOQ6RClsm8oqhSYQ
    _key: '!actors.items!kyWg24EHhvL6kVpJ.rOQ6RClsm8oqhSYQ'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Environment: Any'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: swPDk9g1awP2z2Fy
    _key: '!actors.items!kyWg24EHhvL6kVpJ.swPDk9g1awP2z2Fy'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Organization: Solitary, Pair, Or Flock (3-14)'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: QICKpI8YIlNlR9tc
    _key: '!actors.items!kyWg24EHhvL6kVpJ.QICKpI8YIlNlR9tc'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Treasure: None'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: n8PXwwA2eEtrMHvT
    _key: '!actors.items!kyWg24EHhvL6kVpJ.n8PXwwA2eEtrMHvT'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Constant Vigilance
    system:
      abilityType: su
      description:
        value: >-
          An arbiter gains a +4 bonus to recognize and disbelieve illusions
          created by creatures with the chaotic subtype or possessing the
          chaotic descriptor.
      subType: classFeat
    type: feat
  - _id: SQugeeUd0DE7EsGY
    _key: '!actors.items!kyWg24EHhvL6kVpJ.SQugeeUd0DE7EsGY'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Electrical Burst
    system:
      abilityType: ex
      description:
        value: >-
          An arbiter can release electrical energy from its body in a
          10-foot-radius burst that deals 3d6 electricity damage (DC 13 Reflex
          half). Immediately following such a burst, the arbiter becomes stunned
          for 24 hours. The save DC is Constitution-based.
      subType: classFeat
    type: feat
  - _id: SEoKHQg1T52QyvV8
    _key: '!actors.items!kyWg24EHhvL6kVpJ.SEoKHQg1T52QyvV8'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Locate Inevitable
    system:
      abilityType: su
      description:
        value: >-
          An arbiter can always sense the direction of the nearest non-arbiter
          inevitable on the plane, the better to help it report back to its
          superiors. It cannot sense the range to this inevitable.
      subType: classFeat
    type: feat
  - _id: 6KU7w11HM7Ejkrr3
    _key: '!actors.items!kyWg24EHhvL6kVpJ.6KU7w11HM7Ejkrr3'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: sbc | Conversion Buff
    system:
      active: true
      changes:
        - _id: fkhkpvi6
          formula: '3'
          target: cmb
          type: untypedPerm
        - _id: vew6bwal
          formula: '3'
          target: fort
          type: untypedPerm
        - _id: ch7qt9i4
          formula: '-3'
          target: ref
          type: untypedPerm
      description:
        value: >-
          <h2>sbc | Conversion Buff</h2><p>This Buff was created by
          <strong>sbc</strong> to compensate for differences between the input
          and the values FoundryVTT calculates automatically.</p><p>Especially
          when the pathfinder system gets upgraded, entries in compendiums get
          updated or foundry changes in some way, this buff may become
          outdated.</p><p>For most mooks the conversion should more or less
          work, but for important NPCs or creatures it is adviced to double
          check the conversion manually.</p>
      hideFromToken: true
      subType: perm
      tag: sbcConversionBuff
    type: buff
name: Arbiter
system:
  abilities:
    cha:
      value: 14
    con:
      value: 14
    dex:
      value: 16
    int:
      value: 11
    str:
      value: 11
    wis:
      value: 11
  attributes:
    naturalAC: '+1'
    speed:
      fly:
        base: 50
      land:
        base: 20
    spells:
      spellbooks:
        primary:
          name: Primary
        secondary:
          name: Secondary
        spelllike:
          name: Spell-likes
        tertiary:
          name: Tertiary
    sr:
      formula: '13'
  details:
    alignment: ln
    cr:
      base: 2
    notes:
      value: "\n        <div class=\"statblockContainer\" style=\"margin-top: 15px\"><div style=\"width: 100%; background-color: #ffe9c7; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); columns: 2 150px\">\r\n    <hr style=\"margin-left: 0; margin-top:-2px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n    <div style=\"padding: 5px;\">\r\n        <h1>Arbiter CR 2 </h1>\r\n        \r\n        <!-- BASE -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <strong>Source</strong>: Bestiary 2 pg. 162<br/>\r\n            <strong>XP 600</strong><br/>\r\n            \r\n            \r\n            \r\n            \r\n            \r\n            LN \r\n            Tiny \r\n            Outsider (Extraplanar, Inevitable, Lawful) <br/>\r\n            <strong>Init</strong> 3;\r\n            <strong>Senses</strong> darkvision 60 ft., detect chaos, low-light vision; Perception +5; \r\n            \r\n        </div>\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Defense</h3>\r\n                <strong>AC</strong> 16, <strong>Touch</strong> 15, <strong>Flat-Footed</strong> 13 (+3 Dex, +1 natural, +2 size)<br/>\r\n            <strong>Hit Points</strong> 15 (2 HD; 2d10+4);  regeneration 2 (chaotic)<br/>\r\n            <strong>Fort</strong> 5, <strong>Ref</strong> 3, <strong>Will</strong> 3\r\n            <br/><strong>Defensive Abilities</strong> constant vigilance, constructed; \r\n            \r\n            \r\n            \r\n            <strong>SR</strong> 13\r\n            \r\n            \r\n        </div>\r\n        <div>\r\n            <div style=\"break-inside: avoid;\">\r\n                <h3>Offense</h3>\r\n                        <strong>Speed</strong> 50 ft. (average)<br/>\r\n                <strong>Melee</strong> short sword +7 (1d3/19-20)<br/>\r\n                \r\n                \r\n                \r\n                \r\n            </div>\r\n        </div>\r\n        <!-- SPELLCASTING -->\r\n                \r\n        <!-- TACTICS -->\r\n                \r\n                \r\n                \r\n                \r\n        <!-- STATISTICS -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Statistics</h3>\r\n                <strong>Str</strong> 11, <strong>Dex</strong> 16, <strong>Con</strong> 14, <strong>Int</strong> 11, <strong>Wis</strong> 11, <strong>Cha</strong> 14<br/>\r\n            <strong>Base Atk</strong> +2; <strong>CMB</strong> +3; <strong>CMD</strong> 13<br/>\r\n            <strong>Feats</strong> Flyby Attack, Weapon FinesseB<br/>\r\n            <strong>Skills</strong> Diplomacy +7, Fly +12, Knowledge (planes) +5, Perception +5, Sense Motive +5, Stealth +16<br/>\r\n            <strong>Languages</strong> truespeech<br/>\r\n            \r\n            \r\n            \r\n            \r\n        </div>\r\n        <!-- ECOLOGY -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Ecology</h3>                <strong>Environment:</strong>  any<br/>\r\n                <strong>Organization:</strong>  solitary, pair, or flock (3-14)<br/>\r\n                <strong>Treasure:</strong>  none<br/>\r\n            </div>\r\n        <!-- SPECIAL ABILITIES -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Special Abilities</h3>                <div style='white-space: pre-wrap;'>Constant Vigilance (su): An arbiter gains a +4 bonus to recognize and disbelieve illusions created by creatures with the chaotic subtype or possessing the chaotic descriptor.\n\nElectrical Burst (ex): An arbiter can release electrical energy from its body in a 10-foot-radius burst that deals 3d6 electricity damage (DC 13 Reflex half). Immediately following such a burst, the arbiter becomes stunned for 24 hours. The save DC is Constitution-based.\n\nLocate Inevitable (su): An arbiter can always sense the direction of the nearest non-arbiter inevitable on the plane, the better to help it report back to its superiors. It cannot sense the range to this inevitable.</div>\r\n            </div>\r\n        \r\n        <!-- DESCRIPTION -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Description</h3>        <div style=\"white-space: pre-wrap\">Stealthy, observant, and frequently persuasive, arbiter inevitables are the scouts and diplomats of the inevitable race. Found throughout the multiverse in courts and on battlefields, arbiters keep a close eye on the forces of chaos and do their best to keep the lawful from straying, while simultaneously winning over the hearts and minds of those who might yet be saved. Though their assorted abilities make them extremely useful, arbiters see themselves less as servants than as advisers and counselors, preferring to ride around on their summoners&#x27; shoulders and help guide their &#x27;partners&#x27; on the path of law. They detest being summoned by chaotic individuals, and when teamed with such a creature, they aren&#x27;t above using Diplomacy to try to influence the summoner&#x27;s friends or refusing to undertake actions that seem contrary to their programming.\n\nAn arbiter who comes across evidence of a significant insurgence of chaos upon a given plane does everything in its power to rally its allies against the dangerous instability, and in situations that are clearly beyond its ability to handle, it may refuse to continue onward until the group agrees to help it reach the nearest greater inevitable and make a full report, or else may travel to Utopia itself and present its urgent information in person.\n\nArbiters typically bear the shapes of tiny clockwork spheres with shiny metal wings. Generally peaceful unless combating true creatures of chaos, arbiters prefer to cast protection from chaos on their allies and use command to make opponents drop their weapons and run. Their most powerful weapon, the ability to release their internal energy as a deadly burst, is reserved for dire need and battles of the utmost service to law, as the resulting period of darkness while they&#x27;re powered down is the only thing that seems to truly scare the tiny automatons.\n\nAn arbiter inevitable can serve a spellcaster as a familiar. Such a spellcaster must be lawful neutral, must be at least caster level 7th, and must have the Improved Familiar feat. Arbiter inevitables measure 1 foot in diameter but are surprisingly heavy, weighing 60 pounds. Their ability to fly on metal wings is as much a supernatural ability as a physical one.\n</div>\r\n                    \r\n    </div>\r\n    <hr style=\"margin-left: 0; margin-bottom: -3px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n</div></div>\n    \n        <br>\n        <hr>\n        <div class=\"rawInputContainer\" style=\"margin-top: 15px;\">\n            <h2>RAW INPUT</h2>\n            <hr>\n            <pre style=\"white-space: pre-wrap; font-size: 10px;\">Arbiter CR 2\nSource Bestiary 2 pg. 162\nXP 600\nLN Tiny outsider (extraplanar, inevitable, lawful)\nInit +3; Senses darkvision 60 ft., detect chaos, low-light vision; Perception +5\nDefense\nAC 16, touch 15, flat-footed 13 (+3 Dex, +1 natural, +2 size)\nhp 15 (2d10+4); regeneration 2 (chaotic)\nFort +5, Ref +3, Will +3\nDefensive Abilities constant vigilance, constructed; SR 13\nOffense\nSpeed 20 ft., fly 50 ft. (average)\nMelee short sword +7 (1d3/19-20)\nSpace 2-1/2 ft., Reach 0 ft.\nSpecial Attacks electrical burst\nSpell-Like Abilities (CL 2nd; concentration +4)\nConstant-detect chaos\n3/day-command (DC 13), make whole, protection from chaos\n1/week-commune (CL 12th, 6 questions)\nStatistics\nStr 11, Dex 16, Con 14, Int 11, Wis 11, Cha 14\nBase Atk +2; CMB +3; CMD 13\nFeats Flyby Attack, Weapon FinesseB\nSkills Diplomacy +7, Fly +12, Knowledge (planes) +5, Perception +5, Sense Motive +5, Stealth +16\nLanguages truespeech\nSQ locate inevitable\nEcology\nEnvironment any\nOrganization solitary, pair, or flock (3-14)\nTreasure none\nSpecial Abilities\nConstant Vigilance (Su) An arbiter gains a +4 bonus to recognize and disbelieve illusions created by creatures with the chaotic subtype or possessing the chaotic descriptor.\n\nElectrical Burst (Ex) An arbiter can release electrical energy from its body in a 10-foot-radius burst that deals 3d6 electricity damage (DC 13 Reflex half). Immediately following such a burst, the arbiter becomes stunned for 24 hours. The save DC is Constitution-based.\n\nLocate Inevitable (Su) An arbiter can always sense the direction of the nearest non-arbiter inevitable on the plane, the better to help it report back to its superiors. It cannot sense the range to this inevitable.\nDescription\nStealthy, observant, and frequently persuasive, arbiter inevitables are the scouts and diplomats of the inevitable race. Found throughout the multiverse in courts and on battlefields, arbiters keep a close eye on the forces of chaos and do their best to keep the lawful from straying, while simultaneously winning over the hearts and minds of those who might yet be saved. Though their assorted abilities make them extremely useful, arbiters see themselves less as servants than as advisers and counselors, preferring to ride around on their summoners' shoulders and help guide their 'partners' on the path of law. They detest being summoned by chaotic individuals, and when teamed with such a creature, they aren't above using Diplomacy to try to influence the summoner's friends or refusing to undertake actions that seem contrary to their programming.\n\nAn arbiter who comes across evidence of a significant insurgence of chaos upon a given plane does everything in its power to rally its allies against the dangerous instability, and in situations that are clearly beyond its ability to handle, it may refuse to continue onward until the group agrees to help it reach the nearest greater inevitable and make a full report, or else may travel to Utopia itself and present its urgent information in person.\n\nArbiters typically bear the shapes of tiny clockwork spheres with shiny metal wings. Generally peaceful unless combating true creatures of chaos, arbiters prefer to cast protection from chaos on their allies and use command to make opponents drop their weapons and run. Their most powerful weapon, the ability to release their internal energy as a deadly burst, is reserved for dire need and battles of the utmost service to law, as the resulting period of darkness while they're powered down is the only thing that seems to truly scare the tiny automatons.\n\nAn arbiter inevitable can serve a spellcaster as a familiar. Such a spellcaster must be lawful neutral, must be at least caster level 7th, and must have the Improved Familiar feat. Arbiter inevitables measure 1 foot in diameter but are surprisingly heavy, weighing 60 pounds. Their ability to fly on metal wings is as much a supernatural ability as a physical one.</pre>\n        </div>\n    "
  skills:
    dip:
      rank: 5
    fly:
      rank: 5
    kpl:
      rank: 2
    per:
      rank: 2
    sen:
      rank: 2
    ste:
      rank: 2
  traits:
    senses:
      custom: darkvision 60 ft., detect chaos, low-light vision; Perception +5
      dv: 60
      sc: 0
      tr: 0
    size: tiny
type: npc

