_id: SxrVrD0fP4rWnTef
_key: '!actors!SxrVrD0fP4rWnTef'
img: icons/svg/mystery-man.svg
items:
  - _id: H8FbMUps5Z0gQdvV
    _key: '!actors.items!SxrVrD0fP4rWnTef.H8FbMUps5Z0gQdvV'
    img: systems/pf1/icons/skills/mech_7.jpg
    name: Construct (Clockwork)
    system:
      bab: high
      description:
        value: >-
          <p>A construct is an animated object or artificially created
          creature.</p><h2>Features</h2><p>A construct has the following
          features.</p><ul><li>d10 Hit Die.</li><li>Base attack bonus equal to
          total Hit Dice (fast progression).</li><li>No good saving
          throws.</li><li>Skill points equal to 2 + Int modifier (minimum 1) per
          Hit Die. However, most constructs are mindless and gain no skill
          points or feats. Constructs do not have any class skills, regardless
          of their&nbsp;<a
          href="https://www.d20pfsrd.com/basics-ability-scores/ability-scores/#Intelligence_Int">Intelligence</a>&nbsp;scores.</li><li>Construct
          Size Bonus Hit Points Fine — Diminutive — Tiny — Small 10 Medium 20
          Large 30 Huge 40 Gargantuan 60 Colossal
          80</li></ul><h2>Traits</h2><p>A construct possesses the following
          traits (unless otherwise noted in a creature’s entry).</p><ul><li>No
          Constitution score. Any DCs or other statistics that rely on a
          Constitution score treat a construct as having a score of 10 (no bonus
          or penalty).</li><li>Low-light vision.</li><li>Darkvision 60
          feet.</li><li>Immunity to all mind-affecting effects (charms,
          compulsions, morale effects, patterns, and
          phantasms).</li><li>Immunity to disease, death effects, necromancy
          effects, paralysis, poison, sleep effects, and
          stunning.</li><li>Cannot heal damage on its own, but often can be
          repaired via exposure to a certain kind of effect (see the creature’s
          description for details) or through the use of the&nbsp;<a
          href="https://www.d20pfsrd.com/bestiary/rules-for-monsters/monster-feats#TOC-Craft-Construct-Item-Creation-"
          rel="nofollow">Craft Construct</a>&nbsp;feat. Constructs can also be
          healed through spells such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/m/make-whole"
          rel="nofollow">make whole</a>. A construct with the fast healing
          special quality still benefits from that quality.</li><li>Not subject
          to&nbsp;<a
          href="https://www.d20pfsrd.com/bestiary/rules-for-monsters/universal-monster-rules#TOC-Ability-Damage-and-Drain-Ex-or-Su-"
          rel="nofollow">ability damage</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/bestiary/rules-for-monsters/universal-monster-rules#TOC-Ability-Damage-and-Drain-Ex-or-Su-"
          rel="nofollow">ability drain</a>, fatigue, exhaustion, energy drain,
          or nonlethal damage.</li><li>Immunity to any effect that requires
          a&nbsp;<a
          href="https://www.d20pfsrd.com/gamemastering/combat#TOC-Fortitude"
          rel="nofollow">Fortitude</a>&nbsp;save (unless the effect also works
          on objects, or is harmless).</li><li>Not at risk of death from massive
          damage. Immediately destroyed when reduced to 0 hit points or
          less.</li><li>A construct cannot be raised or resurrected.</li><li>A
          construct is hard to destroy, and gains bonus hit points based on
          size, as shown on the following table.</li></ul><ul><li>Proficient
          with its natural weapons only, unless generally humanoid in form, in
          which case proficient with any weapon mentioned in its
          entry.</li><li>Proficient with no armor.</li><li>Constructs do not
          breathe, eat, or sleep.</li></ul><h2>Construct Size Bonus Hit
          Points</h2><table style="width: 176px;" border="1"><tbody><tr><td
          style="width: 128px;">Fine</td><td style="width:
          48px;">10</td></tr><tr><td>Diminutive</td><td>10</td></tr><tr><td>Tiny</td><td>10</td></tr><tr><td>Small</td><td>10</td></tr><tr><td>Medium</td><td>20</td></tr><tr><td>Large</td><td>30</td></tr><tr><td>Huge</td><td>40</td></tr><tr><td>Gargantuan</td><td>60</td></tr><tr><td>Colossal</td><td>80</td></tr></tbody></table>
      hd: 10
      hp: 26
      level: 3
      skillsPerLevel: 2
      tag: construct
    type: class
  - _id: m1JjV1GZ7CZPNbsq
    _key: '!actors.items!SxrVrD0fP4rWnTef.m1JjV1GZ7CZPNbsq'
    img: systems/pf1/icons/skills/mech_7.jpg
    name: 'Race: Construct'
    system:
      creatureType: construct
      description:
        value: >-
          sbc | As the statblock did not include a race, a custom one was
          generated.
      subTypes:
        - clockwork
    type: race
  - _id: yfbnQTGxgkJ2hxpW
    _key: '!actors.items!SxrVrD0fP4rWnTef.yfbnQTGxgkJ2hxpW'
    img: systems/pf1/icons/skills/shadow_12.jpg
    name: 'Sense: Darkvision 60ft.'
    system:
      abilityType: ex
      description:
        value: >-
          Darkvision is the extraordinary ability to see with no light source at
          all, out to a range specified for the creature. Darkvision is
          black-and-white only (colors cannot be discerned). It does not allow
          characters to see anything that they could not see otherwise—invisible
          objects are still invisible, and illusions are still visible as what
          they seem to be. Likewise, darkvision subjects a creature to gaze
          attacks normally. The presence of light does not spoil darkvision.
      subType: racial
    type: feat
  - _id: 2xvhdhvqqqQ8tWvH
    _key: '!actors.items!SxrVrD0fP4rWnTef.2xvhdhvqqqQ8tWvH'
    img: systems/pf1/icons/skills/shadow_11.jpg
    name: 'Sense: Low-light Vision'
    system:
      abilityType: ex
      description:
        value: >-
          A creature with low-light vision can see twice as far as a human in
          starlight, moonlight, torchlight, and similar conditions of dim light.
          It retains the ability to distinguish color and detail under these
          conditions.
      subType: racial
    type: feat
  - _id: moY29Bz5KOttibAp
    _key: '!actors.items!SxrVrD0fP4rWnTef.moY29Bz5KOttibAp'
    img: systems/pf1/icons/feats/alertness.jpg
    name: Alertness
    system:
      changes:
        - _id: y6j5m7hy
          formula: '@skills.per.rank >= 10 ? 4 : 2'
          target: skill.per
          type: untyped
        - _id: 821zh8wo
          formula: '@skills.sen.rank >= 10 ? 4 : 2'
          target: skill.sen
          type: untyped
      description:
        value: >-
          <em>You often notice things that others might
          miss.</em></p><p><b>Benefits</b><br/>You get a +2 bonus on Perception
          and Sense Motive skill checks. If you have 10 or more ranks in one of
          these skills, the bonus increases to +4 for that skill.
      tags:
        - General
    type: feat
  - _id: gQ4UyowHSJquvkA7
    _key: '!actors.items!SxrVrD0fP4rWnTef.gQ4UyowHSJquvkA7'
    img: systems/pf1/icons/feats/improved-initiative.jpg
    name: Improved Initiative
    system:
      changes:
        - _id: zy8r4pqn
          formula: '4'
          target: init
          type: untyped
      description:
        value: >-
          <em>Your quick reflexes allow you to react rapidly to
          danger.</em></p><p><b>Benefits</b><br/>You get a +4 bonus on
          initiative checks.</p><p><b>Combat Trick</b><br/>Before rolling
          initiative, you can spend 10 stamina points to use 20 as the number
          rolled.
      tags:
        - Combat
    type: feat
  - _id: XpQYiazpYQU2qcTd
    _key: '!actors.items!SxrVrD0fP4rWnTef.XpQYiazpYQU2qcTd'
    img: systems/pf1/icons/feats/lighting-reflexes.jpg
    name: Lightning Reflexes
    system:
      changes:
        - _id: pk2znxg1
          formula: '2'
          target: ref
          type: untyped
      description:
        value: >-
          <em>You have faster reflexes than
          normal.</em></p><p><b>Benefits</b><br/>You get a +2 bonus on all
          Reflex saving throws.
      tags:
        - General
    type: feat
  - _id: gBnllb9s1GWo9DBR
    _key: '!actors.items!SxrVrD0fP4rWnTef.gBnllb9s1GWo9DBR'
    img: systems/pf1/icons/items/weapons/elven-curve-blade.PNG
    name: Weapon Finesse
    system:
      description:
        value: >-
          <em>You are trained in using your agility in melee combat, as opposed
          to brute strength.</em></p><p><b>Benefits</b><br/>With a light weapon,
          rapier, whip, or spiked chain made for a creature of your size
          category, you may use your Dexterity modifier instead of your Strength
          modifier on attack rolls. If you carry a shield, its armor check
          penalty applies to your attack rolls.</p><p><b>Special</b><br/>Natural
          weapons are considered light weapons.</p><p><b>Combat
          Trick</b><br/>You can spend 2 stamina points to negate the penalty on
          attack rolls from your shield's armor check penalty until the start of
          your next turn.
      tags:
        - Combat
    type: feat
  - _id: c8WioZYZJVihFZVp
    _key: '!actors.items!SxrVrD0fP4rWnTef.c8WioZYZJVihFZVp'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Special Quality: Advice'
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: XYZeV2kHLp5WgK9L
    _key: '!actors.items!SxrVrD0fP4rWnTef.XYZeV2kHLp5WgK9L'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Special Quality: Item Installation'
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: KDE6LAcA3qA8Rq6c
    _key: '!actors.items!SxrVrD0fP4rWnTef.KDE6LAcA3qA8Rq6c'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Special Quality: Swift Reactions'
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: YRvUL1WqXQ0pfGJW
    _key: '!actors.items!SxrVrD0fP4rWnTef.YRvUL1WqXQ0pfGJW'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Special Quality: Winding'
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: YiQCOtPvpkBCoJTx
    _key: '!actors.items!SxrVrD0fP4rWnTef.YiQCOtPvpkBCoJTx'
    img: systems/pf1/icons/items/inventory/monster-head.jpg
    name: Bite
    system:
      actions:
        - _id: zohttlpuqboxdgj6
          ability:
            attack: str
            damage: str
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          attackBonus: '2'
          attackName: Bite
          attackNotes:
            - bite +7 (1d3 plus 1d6 electricity)
          damage:
            parts:
              - formula: 1d3+0
                type:
                  custom: ''
                  values:
                    - slashing
          duration:
            units: inst
          effectNotes:
            - plus [[1d6]] electricity
          img: systems/pf1/icons/items/inventory/monster-head.jpg
          name: Attack
          range:
            units: melee
          save:
            dc: 0
          tag: bite
      subType: natural
    type: attack
  - _id: NoTBH1wd5ALypYfr
    _key: '!actors.items!SxrVrD0fP4rWnTef.NoTBH1wd5ALypYfr'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Advice
    system:
      abilityType: ex
      description:
        value: >-
          Clockwork familiars have an innate understanding of how things work,
          granting their masters a +2 bonus on all Craft and Use Magic Device
          checks.
      subType: classFeat
    type: feat
  - _id: Iyan7PydrnsISZdG
    _key: '!actors.items!SxrVrD0fP4rWnTef.Iyan7PydrnsISZdG'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Item Installation
    system:
      abilityType: ex
      description:
        value: >-
          Each clockwork familiar possesses the ability to carry a magic item in
          its body. This specific item type is chosen at the time of the
          construct’s creation, and cannot be changed. While the creature cannot
          activate or use the item, it gains certain constant abilities from the
          resonant magic fields, and can drain the item’s magic as a free action
          in order to gain additional magical effects. In addition, any
          clockwork construct can drain a single charge or spell level from its
          installed item to heal itself for 1d6 hit points as a standard action.
          Removing a spent item and installing a new one is a full-round action.
      subType: classFeat
    type: feat
  - _id: f4SyMTIkaRGdgdak
    _key: '!actors.items!SxrVrD0fP4rWnTef.f4SyMTIkaRGdgdak'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Potion
    system:
      description:
        value: >-
          The clockwork familiar gains a constant protection from
          good/evil/law/chaos effect (one type only, chosen each time a new
          potion is installed). In addition, a clockwork familiar can drain the
          magic from the potion in order to grant this ability to a creature
          sharing its space. This ability to include others in the protection
          effect lasts for 1 minute per spell level of the potion drained.
      subType: classFeat
    type: feat
  - _id: RBVv3NxD3ha9FYaW
    _key: '!actors.items!SxrVrD0fP4rWnTef.RBVv3NxD3ha9FYaW'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Scroll
    system:
      description:
        value: >-
          The clockwork familiar gains a constant detect magic effect as a
          spell-like ability. Draining magic from a scroll allows the familiar
          to cast a single identify spell on behalf of its master for each spell
          level of the spell inscribed on the scroll- these castings may be
          stored and saved, though a scroll used in this manner becomes
          instantly useless, even if not all spell levels have been drained.
      subType: classFeat
    type: feat
  - _id: UAcHwk50K8GBZIXO
    _key: '!actors.items!SxrVrD0fP4rWnTef.UAcHwk50K8GBZIXO'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Wand
    system:
      description:
        value: >-
          The clockwork familiar gains the ability to spit a glob of acid up to
          30 feet as a ranged touch attack, dealing 1d4 points of damage.
          Draining a charge increases the damage to 2d4 points for a single
          attack. This charge is spent before the attack is rolled.
      subType: classFeat
    type: feat
  - _id: mVz7cUZbMuhVnIbi
    _key: '!actors.items!SxrVrD0fP4rWnTef.mVz7cUZbMuhVnIbi'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: sbc | Conversion Buff
    system:
      active: true
      changes:
        - _id: ocy79o4i
          formula: '2'
          target: cmb
          type: untypedPerm
        - _id: 8fw9clff
          formula: '2'
          target: cmd
          type: untypedPerm
        - _id: 4gw8hwa5
          formula: '2'
          target: ac
          type: dodge
        - _id: bedorfj5
          formula: '-10'
          target: mhp
          type: untypedPerm
        - _id: k84bt1m2
          formula: '-8'
          target: skill.fly
          type: untypedPerm
        - _id: 42fdzlld
          formula: '-2'
          target: skill.per
          type: untypedPerm
        - _id: 8z1mc7ku
          formula: '-2'
          target: skill.sen
          type: untypedPerm
      description:
        value: >-
          <h2>sbc | Conversion Buff</h2><p>This Buff was created by
          <strong>sbc</strong> to compensate for differences between the input
          and the values FoundryVTT calculates automatically.</p><p>Especially
          when the pathfinder system gets upgraded, entries in compendiums get
          updated or foundry changes in some way, this buff may become
          outdated.</p><p>For most mooks the conversion should more or less
          work, but for important NPCs or creatures it is adviced to double
          check the conversion manually.</p>
      hideFromToken: true
      subType: perm
      tag: sbcConversionBuff
    type: buff
name: Clockwork Familiar
system:
  abilities:
    cha:
      value: 11
    dex:
      value: 14
    int:
      value: 11
    wis:
      value: 13
  attributes:
    naturalAC: '+2'
    speed:
      fly:
        base: 50
        maneuverability: perfect
    spells:
      spellbooks:
        primary:
          name: Primary
        secondary:
          name: Secondary
        spelllike:
          name: Spell-likes
        tertiary:
          name: Tertiary
  details:
    cr:
      base: 2
    notes:
      value: "\n        <div class=\"statblockContainer\" style=\"margin-top: 15px\"><div style=\"width: 100%; background-color: #ffe9c7; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); columns: 2 150px\">\r\n    <hr style=\"margin-left: 0; margin-top:-2px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n    <div style=\"padding: 5px;\">\r\n        <h1>Clockwork Familiar CR 2 </h1>\r\n        \r\n        <!-- BASE -->\r\n        <div style=\"break-inside: avoid;\">\r\n            \r\n            <strong>XP 600</strong><br/>\r\n            \r\n            \r\n            \r\n            \r\n            \r\n            N \r\n            Tiny \r\n            Construct (Clockwork) <br/>\r\n            <strong>Init</strong> 6;\r\n            <strong>Senses</strong> darkvision 60 ft., low-light vision; Perception +5; \r\n            \r\n        </div>\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Defense</h3>\r\n                <strong>AC</strong> 18, <strong>Touch</strong> 16, <strong>Flat-Footed</strong> 14 (+2 Dex, +2 dodge, +2 natural, +2 size)<br/>\r\n            <strong>Hit Points</strong> 16 (3 HD; 3d10)<br/>\r\n            <strong>Fort</strong> 1, <strong>Ref</strong> 5, <strong>Will</strong> 2\r\n            \r\n            <strong>DR</strong> 5/adamantine\r\n            <strong>Immune</strong> construct traits; \r\n            <strong>Resist</strong> cold 10, fire 10; \r\n            \r\n            \r\n            <br/><strong>Weaknesses</strong> \r\n        </div>\r\n        <div>\r\n            <div style=\"break-inside: avoid;\">\r\n                <h3>Offense</h3>\r\n                        <strong>Speed</strong> 50 ft. (perfect)<br/>\r\n                <strong>Melee</strong> bite +7 (1d3 plus 1d6 electricity)<br/>\r\n                \r\n                \r\n                \r\n                \r\n            </div>\r\n        </div>\r\n        <!-- SPELLCASTING -->\r\n                \r\n        <!-- TACTICS -->\r\n                \r\n                \r\n                \r\n                \r\n        <!-- STATISTICS -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Statistics</h3>\r\n                <strong>Str</strong> 10, <strong>Dex</strong> 14, <strong>Con</strong> 10, <strong>Int</strong> 11, <strong>Wis</strong> 13, <strong>Cha</strong> 11<br/>\r\n            <strong>Base Atk</strong> +3; <strong>CMB</strong> +3; <strong>CMD</strong> 15<br/>\r\n            <strong>Feats</strong> Alertness, Improved Initiative, Lightning Reflexes, Weapon Finesse<br/>\r\n            <strong>Skills</strong> Fly +19, Perception +5, Sense Motive +3, Stealth +12<br/>\r\n            <strong>Languages</strong> Common<br/>\r\n            \r\n            \r\n            \r\n            \r\n        </div>\r\n        <!-- ECOLOGY -->\r\n                \r\n                \r\n                \r\n        <!-- SPECIAL ABILITIES -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Special Abilities</h3>                <div style='white-space: pre-wrap;'>Advice (ex): Clockwork familiars have an innate understanding of how things work, granting their masters a +2 bonus on all Craft and Use Magic Device checks.\n\nItem Installation (ex): Each clockwork familiar possesses the ability to carry a magic item in its body. This specific item type is chosen at the time of the construct’s creation, and cannot be changed. While the creature cannot activate or use the item, it gains certain constant abilities from the resonant magic fields, and can drain the item’s magic as a free action in order to gain additional magical effects. In addition, any clockwork construct can drain a single charge or spell level from its installed item to heal itself for 1d6 hit points as a standard action. Removing a spent item and installing a new one is a full-round action.\n\nPotion: The clockwork familiar gains a constant protection from good/evil/law/chaos effect (one type only, chosen each time a new potion is installed). In addition, a clockwork familiar can drain the magic from the potion in order to grant this ability to a creature sharing its space. This ability to include others in the protection effect lasts for 1 minute per spell level of the potion drained.\n\nScroll: The clockwork familiar gains a constant detect magic effect as a spell-like ability. Draining magic from a scroll allows the familiar to cast a single identify spell on behalf of its master for each spell level of the spell inscribed on the scroll- these castings may be stored and saved, though a scroll used in this manner becomes instantly useless, even if not all spell levels have been drained.\n\nWand: The clockwork familiar gains the ability to spit a glob of acid up to 30 feet as a ranged touch attack, dealing 1d4 points of damage. Draining a charge increases the damage to 2d4 points for a single attack. This charge is spent before the attack is rolled.</div>\r\n            </div>\r\n        \r\n        <!-- DESCRIPTION -->\r\n        \r\n                    \r\n    </div>\r\n    <hr style=\"margin-left: 0; margin-bottom: -3px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n</div></div>\n    \n        <br>\n        <hr>\n        <div class=\"rawInputContainer\" style=\"margin-top: 15px;\">\n            <h2>RAW INPUT</h2>\n            <hr>\n            <pre style=\"white-space: pre-wrap; font-size: 10px;\">Clockwork Familiar CR 2\nXP 600\nN Tiny construct (clockwork)\nInit +6; Senses darkvision 60 ft., low-light vision; Perception +5\n\nDEFENSE\nAC 18, touch 16, flat-footed 14 (+2 Dex, +2 dodge, +2 natural, +2 size)\nhp 16 (3d10)\nFort +1, Ref +5, Will +2\nDR 5/adamantine; Immune construct traits; Resist cold 10, fire 10\nWeaknesses vulnerable to electricity\n\nOFFENSE\nSpeed 30 ft., fly 50 ft. (perfect)\nMelee bite +7 (1d3 plus 1d6 electricity)\n\nSTATISTICS\nStr 10, Dex 14, Con 10, Int 11, Wis 13, Cha 11\nBase Atk +3; CMB +3; CMD 15\nFeats Alertness, Improved Initiative, Lightning Reflexes, Weapon Finesse\nSkills Fly +19, Perception +5, Sense Motive +3, Stealth +12\nLanguages Common\nSQ advice, item installation, swift reactions, winding\n\nSPECIAL ABILITIES\nAdvice (Ex) Clockwork familiars have an innate understanding of how things work, granting their masters a +2 bonus on all Craft and Use Magic Device checks.\nItem Installation (Ex) Each clockwork familiar possesses the ability to carry a magic item in its body. This specific item type is chosen at the time of the construct’s creation, and cannot be changed. While the creature cannot activate or use the item, it gains certain constant abilities from the resonant magic fields, and can drain the item’s magic as a free action in order to gain additional magical effects. In addition, any clockwork construct can drain a single charge or spell level from its installed item to heal itself for 1d6 hit points as a standard action. Removing a spent item and installing a new one is a full-round action.\n\nPotion The clockwork familiar gains a constant protection from good/evil/law/chaos effect (one type only, chosen each time a new potion is installed). In addition, a clockwork familiar can drain the magic from the potion in order to grant this ability to a creature sharing its space. This ability to include others in the protection effect lasts for 1 minute per spell level of the potion drained.\nScroll The clockwork familiar gains a constant detect magic effect as a spell-like ability. Draining magic from a scroll allows the familiar to cast a single identify spell on behalf of its master for each spell level of the spell inscribed on the scroll— these castings may be stored and saved, though a scroll used in this manner becomes instantly useless, even if not all spell levels have been drained.\nWand The clockwork familiar gains the ability to spit a glob of acid up to 30 feet as a ranged touch attack, dealing 1d4 points of damage. Draining a charge increases the damage to 2d4 points for a single attack. This charge is spent before the attack is rolled.</pre>\n        </div>\n    "
  skills:
    fly:
      rank: 13
    per:
      rank: 4
    sen:
      rank: 2
    ste:
      rank: 2
  traits:
    dr:
      custom: 5/adamantine
    dv:
      value:
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
        - vulnerableToElectricity
    eres:
      custom: Cold 10; Fire 10
    languages:
      value:
        - common
        - common
        - common
        - common
        - common
        - common
        - common
        - common
        - common
        - common
        - common
        - common
        - common
        - common
        - common
        - common
        - common
    senses:
      custom: darkvision 60 ft., low-light vision; Perception +5
      dv: 60
      sc: 0
      tr: 0
    size: tiny
type: npc

