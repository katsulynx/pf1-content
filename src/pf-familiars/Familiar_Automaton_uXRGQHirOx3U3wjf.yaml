_id: uXRGQHirOx3U3wjf
_key: '!actors!uXRGQHirOx3U3wjf'
img: icons/svg/mystery-man.svg
items:
  - _id: H8FbMUps5Z0gQdvV
    _key: '!actors.items!uXRGQHirOx3U3wjf.H8FbMUps5Z0gQdvV'
    img: systems/pf1/icons/skills/mech_7.jpg
    name: Construct (Automaton, Extraplanar)
    system:
      bab: high
      description:
        value: >-
          <p>A construct is an animated object or artificially created
          creature.</p><h2>Features</h2><p>A construct has the following
          features.</p><ul><li>d10 Hit Die.</li><li>Base attack bonus equal to
          total Hit Dice (fast progression).</li><li>No good saving
          throws.</li><li>Skill points equal to 2 + Int modifier (minimum 1) per
          Hit Die. However, most constructs are mindless and gain no skill
          points or feats. Constructs do not have any class skills, regardless
          of their&nbsp;<a
          href="https://www.d20pfsrd.com/basics-ability-scores/ability-scores/#Intelligence_Int">Intelligence</a>&nbsp;scores.</li><li>Construct
          Size Bonus Hit Points Fine — Diminutive — Tiny — Small 10 Medium 20
          Large 30 Huge 40 Gargantuan 60 Colossal
          80</li></ul><h2>Traits</h2><p>A construct possesses the following
          traits (unless otherwise noted in a creature’s entry).</p><ul><li>No
          Constitution score. Any DCs or other statistics that rely on a
          Constitution score treat a construct as having a score of 10 (no bonus
          or penalty).</li><li>Low-light vision.</li><li>Darkvision 60
          feet.</li><li>Immunity to all mind-affecting effects (charms,
          compulsions, morale effects, patterns, and
          phantasms).</li><li>Immunity to disease, death effects, necromancy
          effects, paralysis, poison, sleep effects, and
          stunning.</li><li>Cannot heal damage on its own, but often can be
          repaired via exposure to a certain kind of effect (see the creature’s
          description for details) or through the use of the&nbsp;<a
          href="https://www.d20pfsrd.com/bestiary/rules-for-monsters/monster-feats#TOC-Craft-Construct-Item-Creation-"
          rel="nofollow">Craft Construct</a>&nbsp;feat. Constructs can also be
          healed through spells such as&nbsp;<a
          href="https://www.d20pfsrd.com/magic/all-spells/m/make-whole"
          rel="nofollow">make whole</a>. A construct with the fast healing
          special quality still benefits from that quality.</li><li>Not subject
          to&nbsp;<a
          href="https://www.d20pfsrd.com/bestiary/rules-for-monsters/universal-monster-rules#TOC-Ability-Damage-and-Drain-Ex-or-Su-"
          rel="nofollow">ability damage</a>,&nbsp;<a
          href="https://www.d20pfsrd.com/bestiary/rules-for-monsters/universal-monster-rules#TOC-Ability-Damage-and-Drain-Ex-or-Su-"
          rel="nofollow">ability drain</a>, fatigue, exhaustion, energy drain,
          or nonlethal damage.</li><li>Immunity to any effect that requires
          a&nbsp;<a
          href="https://www.d20pfsrd.com/gamemastering/combat#TOC-Fortitude"
          rel="nofollow">Fortitude</a>&nbsp;save (unless the effect also works
          on objects, or is harmless).</li><li>Not at risk of death from massive
          damage. Immediately destroyed when reduced to 0 hit points or
          less.</li><li>A construct cannot be raised or resurrected.</li><li>A
          construct is hard to destroy, and gains bonus hit points based on
          size, as shown on the following table.</li></ul><ul><li>Proficient
          with its natural weapons only, unless generally humanoid in form, in
          which case proficient with any weapon mentioned in its
          entry.</li><li>Proficient with no armor.</li><li>Constructs do not
          breathe, eat, or sleep.</li></ul><h2>Construct Size Bonus Hit
          Points</h2><table style="width: 176px;" border="1"><tbody><tr><td
          style="width: 128px;">Fine</td><td style="width:
          48px;">10</td></tr><tr><td>Diminutive</td><td>10</td></tr><tr><td>Tiny</td><td>10</td></tr><tr><td>Small</td><td>10</td></tr><tr><td>Medium</td><td>20</td></tr><tr><td>Large</td><td>30</td></tr><tr><td>Huge</td><td>40</td></tr><tr><td>Gargantuan</td><td>60</td></tr><tr><td>Colossal</td><td>80</td></tr></tbody></table>
      hd: 10
      hp: 16
      level: 3
      skillsPerLevel: 2
      tag: construct
    type: class
  - _id: uXlNIfoKwpVO2PZL
    _key: '!actors.items!uXRGQHirOx3U3wjf.uXlNIfoKwpVO2PZL'
    img: systems/pf1/icons/skills/mech_7.jpg
    name: 'Race: Construct'
    system:
      creatureType: construct
      description:
        value: >-
          sbc | As the statblock did not include a race, a custom one was
          generated.
      subTypes:
        - automaton
        - extraplanar
    type: race
  - _id: 2IF51LvfVxasbztZ
    _key: '!actors.items!uXRGQHirOx3U3wjf.2IF51LvfVxasbztZ'
    img: systems/pf1/icons/skills/shadow_12.jpg
    name: 'Sense: Darkvision 120ft.'
    system:
      abilityType: ex
      description:
        value: >-
          Darkvision is the extraordinary ability to see with no light source at
          all, out to a range specified for the creature. Darkvision is
          black-and-white only (colors cannot be discerned). It does not allow
          characters to see anything that they could not see otherwise—invisible
          objects are still invisible, and illusions are still visible as what
          they seem to be. Likewise, darkvision subjects a creature to gaze
          attacks normally. The presence of light does not spoil darkvision.
      subType: racial
    type: feat
  - _id: OYwYUhtxUBQ5YGU7
    _key: '!actors.items!uXRGQHirOx3U3wjf.OYwYUhtxUBQ5YGU7'
    img: systems/pf1/icons/skills/shadow_11.jpg
    name: 'Sense: Low-light Vision'
    system:
      abilityType: ex
      description:
        value: >-
          A creature with low-light vision can see twice as far as a human in
          starlight, moonlight, torchlight, and similar conditions of dim light.
          It retains the ability to distinguish color and detail under these
          conditions.
      subType: racial
    type: feat
  - _id: aEDvTHyQofVnQJnV
    _key: '!actors.items!uXRGQHirOx3U3wjf.aEDvTHyQofVnQJnV'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Special Quality: Automaton Core'
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: yyv7iKkUhCW7yYMP
    _key: '!actors.items!uXRGQHirOx3U3wjf.yyv7iKkUhCW7yYMP'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Special Quality: Intelligent Construct'
    system:
      description:
        value: >-
          sbc | Placeholder for Special Qualities, which in most statblocks are
          listed under SQ in the statistics block, but described in the Special
          Abilities. Remove duplicates as needed!
      subType: misc
    type: feat
  - _id: zYJR8vl1oeBwVEim
    _key: '!actors.items!uXRGQHirOx3U3wjf.zYJR8vl1oeBwVEim'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Fast Healing 2
    system:
      description:
        value: sbc | Placeholder
      subType: misc
    type: feat
  - _id: Ju1Aja9bbvrBANdJ
    _key: '!actors.items!uXRGQHirOx3U3wjf.Ju1Aja9bbvrBANdJ'
    img: systems/pf1/icons/items/inventory/monster-head.jpg
    name: Bite
    system:
      actions:
        - _id: cddosw7tyealcwmk
          ability:
            attack: str
            damage: str
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          attackBonus: '1'
          attackName: Bite
          attackNotes:
            - bite +5 (1d3-1)
          damage:
            parts:
              - formula: 1d3+0
                type:
                  custom: ''
                  values:
                    - slashing
          duration:
            units: inst
          img: systems/pf1/icons/items/inventory/monster-head.jpg
          name: Attack
          range:
            units: melee
          save:
            dc: 0
          tag: bite
      subType: natural
    type: attack
  - _id: ICo8yoXiu616uXT4
    _key: '!actors.items!uXRGQHirOx3U3wjf.ICo8yoXiu616uXT4'
    img: systems/pf1/icons/items/inventory/monster-paw-bear.jpg
    name: Claws
    system:
      actions:
        - _id: xp0fls12i7b41ra1
          ability:
            attack: str
            damage: str
          actionType: mwak
          activation:
            type: attack
            unchained:
              type: attack
          attackBonus: '1'
          attackName: Claws
          attackNotes:
            - 2 claws +5 (1d2-1)
          damage:
            parts:
              - formula: 1d2+0
                type:
                  custom: ''
                  values:
                    - bludgeoning
                    - slashing
          duration:
            units: inst
          img: systems/pf1/icons/items/inventory/monster-paw-bear.jpg
          name: Attack
          range:
            units: melee
          save:
            dc: 0
          tag: claws
      subType: natural
    type: attack
  - _id: 4ihvpBgmjus51YGz
    _key: '!actors.items!uXRGQHirOx3U3wjf.4ihvpBgmjus51YGz'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Environment: Any (axis)'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: iTUqOc8mOtGMiD9b
    _key: '!actors.items!uXRGQHirOx3U3wjf.iTUqOc8mOtGMiD9b'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Organization: Solitary'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: xoPHMMbmZW9qab6V
    _key: '!actors.items!uXRGQHirOx3U3wjf.xoPHMMbmZW9qab6V'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: 'Treasure: None'
    system:
      description:
        value: >-
          sbc | Here you’ll find information on how the monster fits into the
          world, notes on its ecology and society, and other bits of useful lore
          and flavor that will help you breathe life into the creature when your
          PCs encounter it.
      subType: misc
    type: feat
  - _id: FuGEK6TVFkGcCcgy
    _key: '!actors.items!uXRGQHirOx3U3wjf.FuGEK6TVFkGcCcgy'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: Disease
    system:
      abilityType: ex
      description:
        value: >-
          Arcana fever: Bite-injury; save Fort DC 13; onset 1d3 days; frequency
          1/day; effect 1d3 Dex damage and 1d3 Int damage; cure 2 consecutive
          saves. The save DC is Constitution-based and includes a +2 racial
          bonus.
      subType: classFeat
    type: feat
  - _id: ZdQn0tzxeAsQ2dcO
    _key: '!actors.items!uXRGQHirOx3U3wjf.ZdQn0tzxeAsQ2dcO'
    img: systems/pf1/icons/skills/yellow_36.jpg
    name: sbc | Conversion Buff
    system:
      active: true
      changes:
        - _id: cyz0f3ww
          formula: '14'
          target: mhp
          type: untypedPerm
        - _id: 2hdrgu89
          formula: '5'
          target: fort
          type: untypedPerm
        - _id: b6buwcj5
          formula: '-2'
          target: aac
          type: untypedPerm
        - _id: w8bs12w0
          formula: '-8'
          target: skill.clm
          type: untypedPerm
      description:
        value: >-
          <h2>sbc | Conversion Buff</h2><p>This Buff was created by
          <strong>sbc</strong> to compensate for differences between the input
          and the values FoundryVTT calculates automatically.</p><p>Especially
          when the pathfinder system gets upgraded, entries in compendiums get
          updated or foundry changes in some way, this buff may become
          outdated.</p><p>For most mooks the conversion should more or less
          work, but for important NPCs or creatures it is adviced to double
          check the conversion manually.</p>
      hideFromToken: true
      subType: perm
      tag: sbcConversionBuff
    type: buff
name: Familiar Automaton
system:
  abilities:
    cha:
      value: 14
    con:
      value: 0
    dex:
      value: 15
    int:
      value: 14
    str:
      value: 8
  attributes:
    naturalAC: '+2'
    speed:
      climb:
        base: 30
      land:
        base: 20
    spells:
      spellbooks:
        primary:
          name: Primary
        secondary:
          name: Secondary
        spelllike:
          name: Spell-likes
        tertiary:
          name: Tertiary
  details:
    alignment: ln
    cr:
      base: 2
    notes:
      value: "\n        <div class=\"statblockContainer\" style=\"margin-top: 15px\"><div style=\"width: 100%; background-color: #ffe9c7; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); columns: 2 150px\">\r\n    <hr style=\"margin-left: 0; margin-top:-2px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n    <div style=\"padding: 5px;\">\r\n        <h1>Familiar Automaton CR 2 </h1>\r\n        \r\n        <!-- BASE -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <strong>Source</strong>: Construct Handbook pg. 24<br/>\r\n            <strong>XP 600</strong><br/>\r\n            \r\n            \r\n            \r\n            \r\n            \r\n            LN \r\n            Tiny \r\n            Construct (Automaton, Extraplanar) <br/>\r\n            <strong>Init</strong> 6;\r\n            <strong>Senses</strong> darkvision 120 ft., low-light vision; Perception +6; \r\n            \r\n        </div>\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Defense</h3>\r\n                <strong>AC</strong> 14, <strong>Touch</strong> 12, <strong>Flat-Footed</strong> 12 (+2 Dex, +2 natural)<br/>\r\n            <strong>Hit Points</strong> 15 (3 HD; 3d10);  fast healing 2<br/>\r\n            <strong>Fort</strong> 1, <strong>Ref</strong> 3, <strong>Will</strong> 1\r\n            \r\n            <strong>DR</strong> 5/adamantine\r\n            <strong>Immune</strong> construct traits, electricity; \r\n            <strong>Resist</strong> cold 10, sonic 10; \r\n            \r\n            \r\n            <br/><strong>Weaknesses</strong> \r\n        </div>\r\n        <div>\r\n            <div style=\"break-inside: avoid;\">\r\n                <h3>Offense</h3>\r\n                        <strong>Speed</strong> 30 ft.<br/>\r\n                <strong>Melee</strong> bite +5 (1d3-1 plus disease), 2 claws +5 (1d2-1)<br/>\r\n                \r\n                \r\n                \r\n                \r\n            </div>\r\n        </div>\r\n        <!-- SPELLCASTING -->\r\n                \r\n        <!-- TACTICS -->\r\n                \r\n                \r\n                \r\n                \r\n        <!-- STATISTICS -->\r\n        <div style=\"break-inside: avoid;\">\r\n            <h3>Statistics</h3>\r\n                <strong>Str</strong> 8, <strong>Dex</strong> 15, <strong>Con</strong> 0, <strong>Int</strong> 14, <strong>Wis</strong> 10, <strong>Cha</strong> 14<br/>\r\n            <strong>Base Atk</strong> +3; <strong>CMB</strong> +0; <strong>CMD</strong> 12<br/>\r\n            <strong>Feats</strong> Improved Initiative, Weapon Finesse<br/>\r\n            <strong>Skills</strong> Climb +7, Diplomacy +7, Knowledge (any one) +8, Perception +6, Spellcraft +7, Stealth +8, Use Magic Device +7<br/>\r\n            <strong>Languages</strong> Common, telepathy (touch)<br/>\r\n            \r\n            \r\n            \r\n            \r\n        </div>\r\n        <!-- ECOLOGY -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Ecology</h3>                <strong>Environment:</strong>  any (Axis)<br/>\r\n                <strong>Organization:</strong>  solitary<br/>\r\n                <strong>Treasure:</strong>  none<br/>\r\n            </div>\r\n        <!-- SPECIAL ABILITIES -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Special Abilities</h3>                <div style='white-space: pre-wrap;'>Disease (ex): Arcana fever: Bite-injury; save Fort DC 13; onset 1d3 days; frequency 1/day; effect 1d3 Dex damage and 1d3 Int damage; cure 2 consecutive saves. The save DC is Constitution-based and includes a +2 racial bonus.</div>\r\n            </div>\r\n        \r\n        <!-- DESCRIPTION -->\r\n            <div style='break-inside: avoid;'>\r\n                <h3>Description</h3>        <div style=\"white-space: pre-wrap\">Familiar automatons were the most common of the automatons created within the Jistkan Imperium. The Artificer Conclave found itself overwhelmed with requests to join the group from hundreds of individuals they deemed unworthy of the more powerful automaton frames. Rather than turn these individuals away and risk the ire of so many Jistkans, the Conclave decided instead to transplant the majority of these individuals into familiar automatons. These familiars also served as a way for the Conclave to test new technologies and perfect the creation of automaton cores.\n\nAll interested citizens were accepted to become familiar automatons so long as they could afford their automaton frame and the automaton core, though some were granted free of charge in order to allow further experimentation. As these automatons served as prototypes for a number of different technologies, most of the first few generations of familiars quickly failed and left their cores stranded without a body. The majority of these stranded cores were hidden away or used as parts in further research. Thanks to breakthroughs made with the aid of these cores, future generations of automatons were more stable and had the potential to remain intact indefinitely. A few existing accounts of automaton core creation reference some cores activating before their creation was complete, seemingly activated by the will of an individual lost from an early generation, though no records corroborate these notes.\n\nThe Artificer Conclave experimented with a number of different forms and frames for the familiar automatons. The design of these forms had aesthetics in mind more than function, which led to the construction of especially elaborate familiars. Eventually, they settled on common animal shapes such as birds, dogs, and snakes, though cats quickly became one of the favorites. Wealthy individuals occasionally requested automaton frames modeled after rare creatures such as faerie dragons, imps, or lantern archons. This trend served the Conclave well, as they could charge high prices for more extravagant frames and then use the income they generated to fund the research and construction of more powerful automatons.\n\nA familiar automaton is usually the same size and twice the weight of the creature after which it is modeled. For the most part, nonfeline familiar automatons use the same statistics as the cat-based familiar automaton presented here, but more powerful familiar automatons may have additional abilities better suited for their more unique frames. On rare occasions, a familiar automaton bears spells engraved in markings on its body-usually signature spells of ancient mages, engraved on their forms by request. Some mages today keep these automatons as their personal familiars. A 7th-level lawful spellcaster with the Improved Familiar feat can select a familiar automaton as a familiar. These familiars serve well due to their innate ability to understand magic and magical items.\n\nAlthough most familiar automatons housed lowerranking citizens and worker-artificers, a number of these familiars housed more notable individuals. A number of judge-artificers and priest-artificers intentionally chose to become familiars rather than taking any of the more powerful automaton forms. The reasons for this varied from humility to a desire to escape from the conflict in which the Jistka Imperium was embroiled. Regardless of the reason, these high familiars, as they came to be known, each had their own powerful abilities. Some high familiars of note include the Judge Lord Valmen, who took on the form of an arbiter inevitable, and Urthilan the Star Priestess, who took a frame resembling a cassisian angel.\n\nFamiliar automatons were not designed for combat but nevertheless feature a number of abilities to assist in fights. The most notable ability is the strange disease that each familiar automaton carries. What was originally a flaw of a familiar&#x27;s automaton core eventually became a standard feature. Each of these cores leaks a minimal amount of energy that manifests as a disease known as arcane fever, technically a form of radiation sickness brought on by exposure to this planar energy. In a true twist of irony, the disease seems to be most potent against wizards, as they quickly forget how to prepare their spells as they slip further into the symptoms of the potent fever.\n\nMost familiar automatons do not bother searching for new automaton cores. The nature of a familiar automaton&#x27;s body generally allows for any core to take dominance of the frame and discard the previous mentality; as such, most familiars tend to stay as far away from other cores as possible, content with their existing nature and unwilling to subject their frames to a new psyche. A rare few familiars have learned to maintain their minds across multiple cores, however, and these often work with other automatons to collect more cores, a practice that allows the familiar to live on far longer than it could on its own.\n\nAs they were the most numerous automatons at the fall of the Jistka Imperium, familiar automatons are the most common variety found throughout Golarion. A familiar automaton&#x27;s intelligence usually allows it to avoid detection when it likes, though scholars all over the world report sightings of these automatons in a variety of forms. The technological nature of Numeria allows familiar automatons to hide in plain sight, where onlookers believe the constructs to be curiosities or projects of local tinkerers-though a spellcaster who doesn&#x27;t want to be forcibly separated from her familiar would be careful to keep a familiar automaton out of the eye of the Technic League.\n\nWhile most familiar automatons remain on Golarion, a sizable number reside elsewhere throughout the Great Beyond. The city of Axis houses quite a few familiar automatons, relaxing in peace as they go unnoticed among the strange citizens of the Eternal City. They tend to serve as messengers between the city&#x27;s denizens, especially other automatons, an exception to most who tend to avoid others of their kind. Among champion automatons, familiars serve as chroniclers, recording great battles and sharing them with other champions; among stalkers, familiars usually act as solving to solve feuds or overseeing two hunters staging contests of skill where the familiar automaton serves as an impartial judge.\n\nThe Boneyard also contains a surprising number of familiar automatons. Here, the constructs walk among the souls awaiting judgment, serving as temporary companions for petitioners. The nature of a petitioner&#x27;s memory means that the petitioner usually does not recognize a familiar automaton but still retains a strong association with a beloved pet from its former life. This association provides comfort for especially distressed petitioners. Thanks to their ability to ease wary souls, a few catrina psychopomps establish friendships with familiar automatons, the two working in tandem to make the wait for judgment as painless as possible.\n</div>\r\n                    \r\n    </div>\r\n    <hr style=\"margin-left: 0; margin-bottom: -3px; width: 100%; height: 4px; background-color: #e0a100; border: 1px solid #000; column-span: all;\" />\r\n</div></div>\n    \n        <br>\n        <hr>\n        <div class=\"rawInputContainer\" style=\"margin-top: 15px;\">\n            <h2>RAW INPUT</h2>\n            <hr>\n            <pre style=\"white-space: pre-wrap; font-size: 10px;\">Familiar Automaton CR 2\nSource Construct Handbook pg. 24\nXP 600\nLN Tiny construct (automaton, extraplanar)\nInit +6; Senses darkvision 120 ft., low-light vision; Perception +6\nDefense\nAC 14, touch 12, flat-footed 12 (+2 Dex, +2 natural)\nhp 15 (3d10); fast healing 2\nFort +1, Ref +3, Will +1\nDR 5/adamantine; Immune construct traits, electricity; Resist cold 10, sonic 10\nWeaknesses vulnerable mind\nOffense\nSpeed 20 ft., climb 30 ft.\nMelee bite +5 (1d3-1 plus disease), 2 claws +5 (1d2-1)\nSpace 2-1/2 ft., Reach 0 ft.\nSpell-Like Abilities (CL 3rd; concentration +4)\nAt will-detect magic, invisibility (self only)\n3/day-magic missile\n1/week-plane shift (self plus 5 lbs. of objects only)\nStatistics\nStr 8, Dex 15, Con -, Int 14, Wis 10, Cha 14\nBase Atk +3; CMB +0; CMD 12\nFeats Improved Initiative, Weapon Finesse\nSkills Climb +7, Diplomacy +7, Knowledge (any one) +8, Perception +6, Spellcraft +7, Stealth +8, Use Magic Device +7\nLanguages Common; telepathy (touch)\nSQ automaton core, intelligent construct\nEcology\nEnvironment any (Axis)\nOrganization solitary\nTreasure none\nSpecial Abilities\nDisease (Ex) Arcane fever: Bite-injury; save Fort DC 13; onset 1d3 days; frequency 1/day; effect 1d3 Dex damage and 1d3 Int damage; cure 2 consecutive saves. The save DC is Constitution-based and includes a +2 racial bonus.\nDescription\nFamiliar automatons were the most common of the automatons created within the Jistkan Imperium. The Artificer Conclave found itself overwhelmed with requests to join the group from hundreds of individuals they deemed unworthy of the more powerful automaton frames. Rather than turn these individuals away and risk the ire of so many Jistkans, the Conclave decided instead to transplant the majority of these individuals into familiar automatons. These familiars also served as a way for the Conclave to test new technologies and perfect the creation of automaton cores.\n\nAll interested citizens were accepted to become familiar automatons so long as they could afford their automaton frame and the automaton core, though some were granted free of charge in order to allow further experimentation. As these automatons served as prototypes for a number of different technologies, most of the first few generations of familiars quickly failed and left their cores stranded without a body. The majority of these stranded cores were hidden away or used as parts in further research. Thanks to breakthroughs made with the aid of these cores, future generations of automatons were more stable and had the potential to remain intact indefinitely. A few existing accounts of automaton core creation reference some cores activating before their creation was complete, seemingly activated by the will of an individual lost from an early generation, though no records corroborate these notes.\n\nThe Artificer Conclave experimented with a number of different forms and frames for the familiar automatons. The design of these forms had aesthetics in mind more than function, which led to the construction of especially elaborate familiars. Eventually, they settled on common animal shapes such as birds, dogs, and snakes, though cats quickly became one of the favorites. Wealthy individuals occasionally requested automaton frames modeled after rare creatures such as faerie dragons, imps, or lantern archons. This trend served the Conclave well, as they could charge high prices for more extravagant frames and then use the income they generated to fund the research and construction of more powerful automatons.\n\nA familiar automaton is usually the same size and twice the weight of the creature after which it is modeled. For the most part, nonfeline familiar automatons use the same statistics as the cat-based familiar automaton presented here, but more powerful familiar automatons may have additional abilities better suited for their more unique frames. On rare occasions, a familiar automaton bears spells engraved in markings on its body-usually signature spells of ancient mages, engraved on their forms by request. Some mages today keep these automatons as their personal familiars. A 7th-level lawful spellcaster with the Improved Familiar feat can select a familiar automaton as a familiar. These familiars serve well due to their innate ability to understand magic and magical items.\n\nAlthough most familiar automatons housed lowerranking citizens and worker-artificers, a number of these familiars housed more notable individuals. A number of judge-artificers and priest-artificers intentionally chose to become familiars rather than taking any of the more powerful automaton forms. The reasons for this varied from humility to a desire to escape from the conflict in which the Jistka Imperium was embroiled. Regardless of the reason, these high familiars, as they came to be known, each had their own powerful abilities. Some high familiars of note include the Judge Lord Valmen, who took on the form of an arbiter inevitable, and Urthilan the Star Priestess, who took a frame resembling a cassisian angel.\n\nFamiliar automatons were not designed for combat but nevertheless feature a number of abilities to assist in fights. The most notable ability is the strange disease that each familiar automaton carries. What was originally a flaw of a familiar's automaton core eventually became a standard feature. Each of these cores leaks a minimal amount of energy that manifests as a disease known as arcane fever, technically a form of radiation sickness brought on by exposure to this planar energy. In a true twist of irony, the disease seems to be most potent against wizards, as they quickly forget how to prepare their spells as they slip further into the symptoms of the potent fever.\n\nMost familiar automatons do not bother searching for new automaton cores. The nature of a familiar automaton's body generally allows for any core to take dominance of the frame and discard the previous mentality; as such, most familiars tend to stay as far away from other cores as possible, content with their existing nature and unwilling to subject their frames to a new psyche. A rare few familiars have learned to maintain their minds across multiple cores, however, and these often work with other automatons to collect more cores, a practice that allows the familiar to live on far longer than it could on its own.\n\nAs they were the most numerous automatons at the fall of the Jistka Imperium, familiar automatons are the most common variety found throughout Golarion. A familiar automaton's intelligence usually allows it to avoid detection when it likes, though scholars all over the world report sightings of these automatons in a variety of forms. The technological nature of Numeria allows familiar automatons to hide in plain sight, where onlookers believe the constructs to be curiosities or projects of local tinkerers-though a spellcaster who doesn't want to be forcibly separated from her familiar would be careful to keep a familiar automaton out of the eye of the Technic League.\n\nWhile most familiar automatons remain on Golarion, a sizable number reside elsewhere throughout the Great Beyond. The city of Axis houses quite a few familiar automatons, relaxing in peace as they go unnoticed among the strange citizens of the Eternal City. They tend to serve as messengers between the city's denizens, especially other automatons, an exception to most who tend to avoid others of their kind. Among champion automatons, familiars serve as chroniclers, recording great battles and sharing them with other champions; among stalkers, familiars usually act as solving to solve feuds or overseeing two hunters staging contests of skill where the familiar automaton serves as an impartial judge.\n\nThe Boneyard also contains a surprising number of familiar automatons. Here, the constructs walk among the souls awaiting judgment, serving as temporary companions for petitioners. The nature of a petitioner's memory means that the petitioner usually does not recognize a familiar automaton but still retains a strong association with a beloved pet from its former life. This association provides comfort for especially distressed petitioners. Thanks to their ability to ease wary souls, a few catrina psychopomps establish friendships with familiar automatons, the two working in tandem to make the wait for judgment as painless as possible.</pre>\n        </div>\n    "
  skills:
    clm:
      rank: 8
    dip:
      rank: 5
    kge:
      rank: 6
    per:
      rank: 6
    spl:
      rank: 5
    ste:
      rank: -2
    umd:
      rank: 5
  traits:
    di:
      value:
        - electricity
    dr:
      custom: 5/adamantine
    eres:
      custom: Cold 10;Sonic 10;
    languages:
      value:
        - common
    senses:
      custom: darkvision 120 ft., low-light vision; Perception +6
      dv: 120
      sc: 0
      tr: 0
    size: tiny
type: npc

