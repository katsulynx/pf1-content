_id: 7Lc81tg2g1Q6TMPo
_key: '!journal!7Lc81tg2g1Q6TMPo'
folder: CMTuXqZ6Ywxqk9Ar
name: 7.1.5.01. The Golden Rule
pages:
  - _id: D20thnejSe7nxpJz
    _key: '!journal.pages!7Lc81tg2g1Q6TMPo.D20thnejSe7nxpJz'
    image: {}
    name: 7.1.5.01. The Golden Rule
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.dS0UkKOINqI2iTDX]{(Index)
        Magic}</p><p>@Compendium[pf-content.pf-rules.zCeqoMSKdZ8r4EPd]{7.1.
        Mastering
        Magic}</p><ul><li><p>@Compendium[pf-content.pf-rules.xdyGkidQGN7vYL1E]{7.1.5.
        Designing
        Spells}</p><ul><li><strong>@Compendium[pf-content.pf-rules.7Lc81tg2g1Q6TMPo]{7.1.5.01.
        The Golden
        Rule}</strong></li><li>@Compendium[pf-content.pf-rules.DZBAa9axA5Gv4el0]{7.1.5.02.
        Spell
        Terminology}</li><li>@Compendium[pf-content.pf-rules.dbb5pwAitIa3cqqj]{7.1.5.03.
        Intended
        Level}</li><li>@Compendium[pf-content.pf-rules.1DkXvcE3KMmBww9Z]{7.1.5.04.
        Function}</li><li>@Compendium[pf-content.pf-rules.Xu1jyIRMERPFh8X6]{7.1.5.05.
        Spell
        Research}</li><li>@Compendium[pf-content.pf-rules.fF9HpImlhp8oNX5Q]{7.1.5.06.
        Spell
        Damage}</li><li>@Compendium[pf-content.pf-rules.zrMFTIHfzbQTTwZ1]{7.1.5.07.
        Target}</li><li>@Compendium[pf-content.pf-rules.wsF1rdV2ACkYU3Su]{7.1.5.08.
        Damage
        Caps}</li><li>@Compendium[pf-content.pf-rules.9TyMr3XedNUcWVzB]{7.1.5.09.
        Range}</li><li>@Compendium[pf-content.pf-rules.KlR2PDab6uknvopJ]{7.1.5.10.
        Duration}</li><li>@Compendium[pf-content.pf-rules.x0E7IZ5imhQ5srrf]{7.1.5.11.
        Saving
        Throw}</li><li>@Compendium[pf-content.pf-rules.RRG1A0rBL44Qeskg]{7.1.5.12.
        Spell
        Resistance}</li><li>@Compendium[pf-content.pf-rules.kvl8JOILza5HEj7N]{7.1.5.13.
        Casting
        Time}</li><li>@Compendium[pf-content.pf-rules.wb9tYgBlWX8peqFl]{7.1.5.14.
        Components}</li><li>@Compendium[pf-content.pf-rules.PCvwd6PaQH7mD7o6]{7.1.5.15.
        School}</li><li>@Compendium[pf-content.pf-rules.dmUzb6b2BspbBdDp]{7.1.5.16.
        Bonus
        Type}</li><li>@Compendium[pf-content.pf-rules.gh6sxSeK1RNLLFoN]{7.1.5.17.
        Description}</li><li>@Compendium[pf-content.pf-rules.q6Xva2npOy7fVL42]{7.1.5.18.
        Hierarchy of Attack
        Effects}</li><li>@Compendium[pf-content.pf-rules.s6CWKZDo0zQeRXMJ]{7.1.5.19.
        Depletable
        Statistics}</li><li>@Compendium[pf-content.pf-rules.r8zYCFuq59NEtjkz]{7.1.5.20.
        Core is
        King}</li><li>@Compendium[pf-content.pf-rules.aVUL2wrJszAUzNMK]{7.1.5.21.
        Multipurpose
        Spells}</li><li>@Compendium[pf-content.pf-rules.Ak9Ws3Z16GY6QSoH]{7.1.5.22.
        Choosing
        Descriptors}</li><li>@Compendium[pf-content.pf-rules.baSlaM24kwB68nuf]{7.1.5.23.
        Benchmarks}</li></ul></li></ul><hr /><p><strong>Source</strong>
        <em>Ultimate Magic pg. 128</em></p><p>Compare your spell to similar
        spells, and to other spells of its intended level.</p><p>Unlike when
        pricing magic items, there are no formulae for how to correctly “price”
        a spell. The entire process is a matter of comparing the new spell
        you’re creating to other spells and evaluating whether your spell is
        weaker, stronger, or about the same as that spell or group of spells.
        Designing a spell requires a firm understanding of all the game’s rules,
        not just those related to spells. Furthermore, it requires an
        understanding of some unwritten game assumptions, most of which are
        discussed throughout this section.</p><p>Example: If you look at the
        spell list in the Core Rulebook, you’ll notice that there isn’t a
        1st-level wizard spell that deals sonic damage. You may decide to design
        a spell to fill that niche, modeling it after burning hands, except
        dealing sonic damage instead of fire—perhaps you’d call it sonic
        screech. However, there’s a reason there aren’t as many sonic spells in
        the game: “sonic” as an energy type is a late addition to the rules, and
        very few monsters have any resistance to sonic damage because most
        monsters existed before “sonic” was defined as an energy type. Because
        there are fewer creatures with sonic resistance than creatures with fire
        resistance, sonic screech will almost always be a better spell than
        burning hands. That means if you introduce sonic screech into your game,
        you’ll see savvy players selecting it instead of burning hands. If a new
        spell displaces an existing spell from the roster of most spellcasters,
        it probably means it’s better than the other available choices—and if
        it’s so good that it’s obviously the best spell choice, it’s probably
        overpowered. Understanding the entire system of rules can help you avoid
        mistakes like this.</p>
      format: 1
      markdown: ''
    title:
      level: 1
      show: true
    type: text

