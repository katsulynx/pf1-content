_id: wYDD2ws3dIeQaHKV
_key: '!journal!wYDD2ws3dIeQaHKV'
folder: tfgLGI205b9dxEjx
name: 6.2.2.06. Driving Vehicles
pages:
  - _id: 6Jd06btK4VG68Yq9
    _key: '!journal.pages!wYDD2ws3dIeQaHKV.6Jd06btK4VG68Yq9'
    image: {}
    name: 6.2.2.06. Driving Vehicles
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.4TX2Q1MaWt1B8298]{(Index)
        Items}</p><p>@Compendium[pf-content.pf-rules.F9hzOnUh5c0Ls2p6]{6.2.
        Vehicles}</p><ul><li><p>@Compendium[pf-content.pf-rules.sjmHLtUHjp10PRNg]{6.2.2.
        Full Vehicle
        Rules}</p><ul><li>@Compendium[pf-content.pf-rules.CQgsuMCjnSXQT5tK]{6.2.2.01.
        Drivers}</li><li>@Compendium[pf-content.pf-rules.n20ux7F2Gua3Henu]{6.2.2.02.
        Occupants}</li><li>@Compendium[pf-content.pf-rules.WNrj8ktGtzmJ2idV]{6.2.2.03.
        Propulsion and Driving
        Checks}</li><li>@Compendium[pf-content.pf-rules.bqobhetOALHmOywu]{6.2.2.04.
        Vehicle Size and
        Space}</li><li>@Compendium[pf-content.pf-rules.xPL69LmsvZhMQXkI]{6.2.2.05.
        Vehicle Facing and
        Movement}</li><li><strong>@Compendium[pf-content.pf-rules.wYDD2ws3dIeQaHKV]{6.2.2.06.
        Driving
        Vehicles}</strong></li><li>@Compendium[pf-content.pf-rules.AoRyhHSyrQd2YNsf]{6.2.2.07.
        Optional Rule: Wide
        Turns}</li><li>@Compendium[pf-content.pf-rules.uCzllyIwfv6C8w51]{6.2.2.08.
        Vehicles in
        Combat}</li><li>@Compendium[pf-content.pf-rules.RQx3RwhdQds2znsh]{6.2.2.09.
        Propulsion
        Devies}</li><li>@Compendium[pf-content.pf-rules.UkducnpdR3NCcjdK]{6.2.2.10.
        Driving Devices}</li></ul></li></ul><hr /><p><strong>Source</strong>
        <em>Ultimate Combat pg. 173</em></p><p>Controlling a vehicle takes
        common sense, awareness, intuition, and often some amount of skill in
        its method of propulsion. In the case of muscle propulsion, it is about
        guiding the creatures to move the vehicle. In the case of current
        propulsion, it is about using the current and tools like sails, oars, or
        a rudder to move the vehicle. With magic, it is typically about
        understanding the magic device that powers the propulsion and using the
        device properly.</p><h2>Driving Actions</h2><p>A driver can, at the
        start of her turn, before taking any other action, take any of the
        following actions (except the “uncontrolled” action) to control a
        vehicle. If the driver does not take an action, takes another action, or
        delays or readies an action, she loses control of the vehicle and the
        vehicle takes the “uncontrolled” action.</p><h3>Accelerate (standard
        action)</h3><p>With a successful driving check, the vehicle’s current
        speed increases up to its acceleration (in 5-foot increments; minimum 5
        feet), but no higher than its maximum speed. The vehicle can move
        forward or forward diagonally. In other words, each time a vehicle
        enters a new 5-foot square, it can choose any of its forward-facing
        squares—the ones directly in front or either of the squares directly
        forward and diagonal. This allows the vehicle to swerve. A driver who
        fails her driving check can only move into squares directly in front of
        the vehicle’s forward facing.</p><h3>Decelerate (standard
        action)</h3><p>With a successful driving check, the vehicle’s current
        speed decreases by a rate up to its acceleration (in 5-foot increments;
        minimum 5 feet). On a failed check, the vehicle does not decelerate.
        Either way, the vehicle can move forward diagonally. If deceleration
        reduces a vehicle’s speed to 0, some amount of inertia will continue to
        move the vehicle forward. The vehicle moves forward (either directly
        forward or forward diagonally) 1d4 × 5 feet before coming to a complete
        stop. Having the Expert Driver feat (see page 100) reduces this distance
        by 10 feet (minimum 0 feet).</p><h3>Keep It Going (move
        action)</h3><p>With a successful driving check, the driver can move the
        vehicle forward on its current facing at its current speed, and it can
        move forward diagonally. Failing the check keeps the speed constant, but
        you cannot move the vehicle forward diagonally.</p><h3>Reverse (standard
        action)</h3><p>A vehicle may only be moved in reverse if it is at a full
        stop (movement of 0 feet). On a successful driving check, a vehicle can
        move backward at half its acceleration, moving either directly backward
        (the reverse of its forward facing) or backward diagonally. On a failed
        check, it does not move backward.</p><h3>Turn (standard
        action)</h3><p>The driver takes this action to turn a vehicle’s forward
        facing 90 degrees. The vehicle moves its current speed. If a vehicle’s
        current speed is twice its acceleration, the driving check DC increases
        by 5. If a vehicle’s movement is three times its acceleration, the
        driving check DC increases by 10. If it is four or more times its
        acceleration, the DC increases by 20. With a successful driving check,
        the vehicle changes its facing either left or right by 90 degrees at any
        point during its movement. Do this by pivoting the vehicle so that the
        left rear or right rear side of the vehicle takes the place of the
        vehicle’s former forward facing side. On a failed check, the vehicle
        does not turn, but can be moved forward diagonally during its
        movement.</p><h3>Uncontrolled (no action)</h3><p>When the driver does
        nothing or there is no driver, the vehicle is uncontrolled. An
        uncontrolled vehicle moves forward only (it cannot move forward
        diagonally). If a vehicle has muscle propulsion, it decelerates at a
        rate equal to its acceleration. If a vehicle is powered by an air
        current, water current, or some form of weird current, it slows by 10
        feet. These decelerations are cumulative. If a vehicle does nothing, it
        cannot perform vehicular bull rushes, but can still perform a vehicular
        overrun or a ramming maneuver (see Vehicle Combat
        Maneuvers).</p><h2>Driving a Vehicle Outside of Combat</h2><p>Since
        driving a vehicle outside of combat is easily accomplished by taking 10
        on the skill check, driving checks are not normally needed. Almost every
        character can do it with relative ease; the DCs are given only to
        adjudicate special situations that may come up in your
        game.</p><h2>Driving a Vehicle with Magic Propulsion</h2><p>Typically, a
        vehicle with magic propulsion requires actions but no driving checks in
        order to drive it. When driving a vehicle with magic propulsion, treat
        every action as though the driver succeeded at the driving
        check.</p><h2>Driving a Vehicle without the Proper Skill</h2><p>If a
        driver lacks the proper skill to drive a vehicle, the driver can always
        make a Wisdom ability check instead of the appropriate skill check. The
        driver can even take 10 or gain the benefits of aid another when using
        Wisdom instead of the vehicle’s normal driving skill.</p><h2>Vehicle
        Crews</h2><p>Some vehicles require a crew. A vehicle with a full crew
        complement is as easy to control as any other vehicle. A vehicle without
        a full crew complement, but with at least half its crew, increases all
        driving check DCs by 10. A vehicle needs at least half its crew
        complement in order to be driven at all. Crew members can take no action
        while the vehicle is in motion except to aid in that vehicle’s movement.
        A crew member does not threaten an area.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text

