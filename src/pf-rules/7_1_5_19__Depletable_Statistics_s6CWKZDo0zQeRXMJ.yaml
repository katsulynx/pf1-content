_id: s6CWKZDo0zQeRXMJ
_key: '!journal!s6CWKZDo0zQeRXMJ'
folder: CMTuXqZ6Ywxqk9Ar
name: 7.1.5.19. Depletable Statistics
pages:
  - _id: PVCsTxO8w8pBjgzy
    _key: '!journal.pages!s6CWKZDo0zQeRXMJ.PVCsTxO8w8pBjgzy'
    image: {}
    name: 7.1.5.19. Depletable Statistics
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.dS0UkKOINqI2iTDX]{(Index)
        Magic}</p><p>@Compendium[pf-content.pf-rules.zCeqoMSKdZ8r4EPd]{7.1.
        Mastering
        Magic}</p><ul><li><p>@Compendium[pf-content.pf-rules.xdyGkidQGN7vYL1E]{7.1.5.
        Designing
        Spells}</p><ul><li>@Compendium[pf-content.pf-rules.7Lc81tg2g1Q6TMPo]{7.1.5.01.
        The Golden
        Rule}</li><li>@Compendium[pf-content.pf-rules.DZBAa9axA5Gv4el0]{7.1.5.02.
        Spell
        Terminology}</li><li>@Compendium[pf-content.pf-rules.dbb5pwAitIa3cqqj]{7.1.5.03.
        Intended
        Level}</li><li>@Compendium[pf-content.pf-rules.1DkXvcE3KMmBww9Z]{7.1.5.04.
        Function}</li><li>@Compendium[pf-content.pf-rules.Xu1jyIRMERPFh8X6]{7.1.5.05.
        Spell
        Research}</li><li>@Compendium[pf-content.pf-rules.fF9HpImlhp8oNX5Q]{7.1.5.06.
        Spell
        Damage}</li><li>@Compendium[pf-content.pf-rules.zrMFTIHfzbQTTwZ1]{7.1.5.07.
        Target}</li><li>@Compendium[pf-content.pf-rules.wsF1rdV2ACkYU3Su]{7.1.5.08.
        Damage
        Caps}</li><li>@Compendium[pf-content.pf-rules.9TyMr3XedNUcWVzB]{7.1.5.09.
        Range}</li><li>@Compendium[pf-content.pf-rules.KlR2PDab6uknvopJ]{7.1.5.10.
        Duration}</li><li>@Compendium[pf-content.pf-rules.x0E7IZ5imhQ5srrf]{7.1.5.11.
        Saving
        Throw}</li><li>@Compendium[pf-content.pf-rules.RRG1A0rBL44Qeskg]{7.1.5.12.
        Spell
        Resistance}</li><li>@Compendium[pf-content.pf-rules.kvl8JOILza5HEj7N]{7.1.5.13.
        Casting
        Time}</li><li>@Compendium[pf-content.pf-rules.wb9tYgBlWX8peqFl]{7.1.5.14.
        Components}</li><li>@Compendium[pf-content.pf-rules.PCvwd6PaQH7mD7o6]{7.1.5.15.
        School}</li><li>@Compendium[pf-content.pf-rules.dmUzb6b2BspbBdDp]{7.1.5.16.
        Bonus
        Type}</li><li>@Compendium[pf-content.pf-rules.gh6sxSeK1RNLLFoN]{7.1.5.17.
        Description}</li><li>@Compendium[pf-content.pf-rules.q6Xva2npOy7fVL42]{7.1.5.18.
        Hierarchy of Attack
        Effects}</li><li><strong>@Compendium[pf-content.pf-rules.s6CWKZDo0zQeRXMJ]{7.1.5.19.
        Depletable
        Statistics}</strong></li><li>@Compendium[pf-content.pf-rules.r8zYCFuq59NEtjkz]{7.1.5.20.
        Core is
        King}</li><li>@Compendium[pf-content.pf-rules.aVUL2wrJszAUzNMK]{7.1.5.21.
        Multipurpose
        Spells}</li><li>@Compendium[pf-content.pf-rules.Ak9Ws3Z16GY6QSoH]{7.1.5.22.
        Choosing
        Descriptors}</li><li>@Compendium[pf-content.pf-rules.baSlaM24kwB68nuf]{7.1.5.23.
        Benchmarks}</li></ul></li></ul><hr /><p><strong>Source</strong>
        <em>Ultimate Magic pg. 135</em></p><p>Depletable statistics are any
        values in a creature or object’s stat block that can be reduced by some
        form of attack and that can neutralize, kill, or destroy the creature or
        object when they reach a low value (typically 0). Hit points, ability
        scores, and levels are all depletable statistics—a creature falls
        unconscious below 0 hit points and eventually dies; objects, undead, and
        constructs are destroyed at 0 hit points; creatures are made helpless or
        killed by bringing an ability score to 0; creatures die when their
        negative levels equal their total Hit Dice. Many magical attacks and
        most nonmagical attacks reduce a target’s depletable statistics in some
        way, eventually defeating the target.</p><p>However, attack bonuses,
        saving throw bonuses, Armor Class, hardness, CMB, CMD, initiative,
        speed, skill modifiers, and most other game statistics are not
        depletable statistics because no matter how low these statistics get,
        the affected creature or object is still able to take actions. For
        example, a spell that gives a target a –10 attack penalty has little
        effect on a sorcerer casting fireball, as would a spell that gave her a
        –10 penalty on her Will saving throws; despite her poor attack rolls and
        miserable Will saves, she is still quite capable of blasting her
        opponents to bits, whether these penalties are –10 or –100. Similarly, a
        fighter with a –10 penalty on Fortitude saving throws can still swing a
        sword, as can one with a –10 penalty to Armor Class; the fighter is
        still viable despite these penalties.</p><p>“Depletable statistic” isn’t
        an official game term, but it is a helpful concept when comparing power
        levels of spells—attacking depletable statistics is a war of attrition
        that can eventually wear down the target, whereas adding penalties to
        non-depletable statistics may have no effect at all, as the target may
        have other attacks that allow them to ignore those penalties.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text

