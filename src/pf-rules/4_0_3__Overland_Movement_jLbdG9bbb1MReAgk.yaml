_id: jLbdG9bbb1MReAgk
_key: '!journal!jLbdG9bbb1MReAgk'
folder: pqVHetlXys8NATtP
name: 4.0.3. Overland Movement
pages:
  - _id: PsQOsL9GrgPwr1xw
    _key: '!journal.pages!jLbdG9bbb1MReAgk.PsQOsL9GrgPwr1xw'
    image: {}
    name: 4.0.3. Overland Movement
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.kAJexDuJ2IAXZODj]{(Index)
        Exploration}</p><p><strong>@Compendium[pf-content.pf-rules.vsp6TyvvUw3KOD0b]{4.0.
        Movement}</strong></p><p>@Compendium[pf-content.pf-rules.TrYHWkJ3jiOCYufl]{4.1.
        Vision and
        Light}</p><p>@Compendium[pf-content.pf-rules.1EvBiiUnVOT4cUbT]{4.2.
        Breaking and Entering}</p><hr /><p><strong>Source</strong>: PRPG Core
        Rulebook pg. 171</p><p>Characters covering long distances cross-country
        use overland movement. Overland movement is measured in miles per hour
        or miles per day. A day represents 8 hours of actual travel time. For
        rowed watercraft, a day represents 10 hours of rowing. For a sailing
        ship, it represents 24 hours.</p><h2>Walk</h2><p>A character can walk 8
        hours in a day of travel without a problem. Walking for longer than that
        can wear him out (see Forced March, below).</p><h2>Hustle</h2><p>A
        character can hustle for 1 hour without a problem. Hustling for a second
        hour in between sleep cycles deals 1 point of nonlethal damage, and each
        additional hour deals twice the damage taken during the previous hour of
        hustling. A character who takes any nonlethal damage from hustling
        becomes fatigued.</p><p>A fatigued character can’t run or charge and
        takes a penalty of –2 to Strength and Dexterity. Eliminating the
        nonlethal damage also eliminates the fatigue.</p><h2>Run</h2><p>A
        character can’t run for an extended period of time. Attempts to run and
        rest in cycles effectively work out to a
        hustle.</p><h2>Terrain</h2><p>The terrain through which a character
        travels affects the distance he can cover in an hour or a day (see Table
        7–8). A highway is a straight, major, paved road. A road is typically a
        dirt track. A trail is like a road, except that it allows only
        single-file travel and does not benefit a party traveling with vehicles.
        Trackless terrain is a wild area with no paths.</p><h2>Forced
        March</h2><p>In a day of normal walking, a character walks for 8 hours.
        The rest of the daylight time is spent making and breaking camp,
        resting, and eating.</p><p>A character can walk for more than 8 hours in
        a day by making a forced march. For each hour of marching beyond 8
        hours, a Constitution check (DC 10, +2 per extra hour) is required. If
        the check fails, the character takes 1d6 points of nonlethal damage. A
        character who takes any nonlethal damage from a forced march becomes
        fatigued. Eliminating the nonlethal damage also eliminates the fatigue.
        It’s possible for a character to march into unconsciousness by pushing
        himself too hard.</p><h2>Mounted Movement</h2><p>A mount bearing a rider
        can move at a hustle. The damage it takes when doing so, however, is
        lethal damage, not nonlethal damage. The creature can also be ridden in
        a forced march, but its Constitution checks automatically fail, and the
        damage it takes is lethal damage. Mounts also become fatigued when they
        take any damage from hustling or forced marches.</p><p>See Table 7–9:
        Mounts and Vehicles for mounted speeds and speeds for vehicles pulled by
        draft animals.</p><h2>Waterborne Movement</h2><p>See Table 7–9: Mounts
        and Vehicles for speeds for water vehicles.</p><h2>Table 7-8: Terrain
        and Overland
        Movement</h2><table><thead><tr><td>Terrain</td><td>Highway</td><td>Road
        or Trail</td><td>Trackless</td></tr></thead><tbody><tr><td>Desert,
        sandy</td><td>×1</td><td>×1/2</td><td>×1/2</td></tr><tr><td>Forest</td><td>×1</td><td>×1</td><td>×1/2</td></tr><tr><td>Hills</td><td>×1</td><td>×3/4</td><td>×1/2</td></tr><tr><td>Jungle</td><td>×1</td><td>×3/4</td><td>×1/4</td></tr><tr><td>Moor</td><td>×1</td><td>×1</td><td>×3/4</td></tr><tr><td>Mountains</td><td>×3/4</td><td>×3/4</td><td>×1/2</td></tr><tr><td>Plains</td><td>×1</td><td>×1</td><td>×3/4</td></tr><tr><td>Swamp</td><td>×1</td><td>×3/4</td><td>×1/2</td></tr><tr><td>Tundra,
        frozen</td><td>×1</td><td>×3/4</td><td>×3/4</td></tr></tbody></table><h2>Table
        7-9: Mounts and
        Vehicles</h2><table><thead><tr><td>Mount/Vehicle</td><td>Per
        Hour</td><td>Per Day</td></tr></thead><tbody><tr><td><strong>Mount
        (carrying load)</strong></td><td> </td></tr><tr><td>Light
        horse</td><td>5miles</td><td>40 miles</td></tr><tr><td>Light horse
        (175-525 lbs.)<sup>1</sup></td><td>3-1/2miles</td><td>28
        miles</td></tr><tr><td>Heavy horse</td><td>5 miles</td><td>40
        miles</td></tr><tr><td>Heavy Horse (229-690
        lbs.)<sup>1</sup></td><td>3-1/2 miles</td><td>28
        miles</td></tr><tr><td>Pony</td><td>4 miles</td><td>32
        miles</td></tr><tr><td>Pony (151-450 lbs.)<sup>1</sup></td><td>3
        miles</td><td>24 miles</td></tr><tr><td>Dog, riding</td><td>4
        miles</td><td>32 miles</td></tr><tr><td>Dog, riding (101-300
        lbs.)<sup>1</sup></td><td>3 miles</td><td>24 miles</td></tr><tr><td>Cart
        or wagon</td><td>2 miles</td><td>16
        miles</td></tr><tr><td><strong>Ship</strong></td><td>
        </td></tr><tr><td>Raft or barge (poled or towed)<sup>2</sup></td><td>1/2
        mile</td><td>5 miles</td></tr><tr><td>Keelboat
        (rowed)<sup>2</sup></td><td>1 mile</td><td>10
        miles</td></tr><tr><td>Rowboat (rowed)<sup>2</sup></td><td>1-1/2
        miles</td><td>15 miles</td></tr><tr><td>Sailing ship (sailed)</td><td>2
        miles</td><td>48 miles</td></tr><tr><td>Warship (sailed and
        rowed)</td><td>2-1/2 miles</td><td>60 miles</td></tr><tr><td>Longship
        (sailed and rowed)</td><td>3 miles</td><td>72
        miles</td></tr><tr><td>Galley (rowed and sailed)</td><td>4
        miles</td><td>96 miles</td></tr></tbody></table><p>1 Quadrupeds, such as
        horses, can carry heavier load than characters can. See Carrying
        Capacity on page 171 for more information.<br />2 Rafts, barges,
        keelboats, and rowboats are most often used on lakes and rivers. If
        going downstream, add the speed of the current (typically 3 miles per
        hour) to the speed of the vehicle. In addition to 10 hours of being
        rowed, the vehicle can also float an additional 14 hours, if someone can
        guide it, adding an additional 42 miles to the daily distance traveled.
        These vehicles can’t be rowed against any significant current, but they
        can be pulled upstream by draft animals on the shores.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text

