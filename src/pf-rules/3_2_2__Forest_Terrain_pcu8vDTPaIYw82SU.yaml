_id: pcu8vDTPaIYw82SU
_key: '!journal!pcu8vDTPaIYw82SU'
folder: br8fwJFrbgshQXJz
name: 3.2.2. Forest Terrain
pages:
  - _id: NtRgWPpDmRQqleMM
    _key: '!journal.pages!pcu8vDTPaIYw82SU.NtRgWPpDmRQqleMM'
    image: {}
    name: 3.2.2. Forest Terrain
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.XOJpjqmBzwE4R4n0]{(Index)
        Environment}</p><p>@Compendium[pf-content.pf-rules.CPUpraqn1jz9vhMh]{3.2.
        Wilderness}</p><ul><li>@Compendium[pf-content.pf-rules.QrderWyxAp6RUvtz]{3.2.1.
        Getting
        Lost}</li><li><strong>@Compendium[pf-content.pf-rules.pcu8vDTPaIYw82SU]{3.2.2.
        Forest
        Terrain}</strong></li><li>@Compendium[pf-content.pf-rules.zJ4qqxSB0Iufd5Be]{3.2.3.
        Marsh
        Terrain}</li><li>@Compendium[pf-content.pf-rules.RVKULXygOAXTZ1WC]{3.2.4.
        Hills
        Terrain}</li><li>@Compendium[pf-content.pf-rules.B5qjfHf1pFgEA7bg]{3.2.5.
        Mountain
        Terrain}</li><li>@Compendium[pf-content.pf-rules.tuZEOzPezZUbz4Fj]{3.2.5.2.
        Mountain
        Travel}</li><li>@Compendium[pf-content.pf-rules.3YyU7Yg6wbShgOKM]{3.2.6.
        Desert
        Terrain}</li><li>@Compendium[pf-content.pf-rules.QOCypcVHE6zpYlyD]{3.2.7.
        Plains
        Terrain}</li><li>@Compendium[pf-content.pf-rules.xiRC5p84P3tFW9mo]{3.2.8.
        Aquatic Terrain}</li></ul><hr /><p><strong>Source</strong>: PRPG Core
        Rulebook pg. 425</p><p>Forest terrain can be divided into three
        categories: sparse, medium, and dense. An immense forest could have all
        three categories within its borders, with more sparse terrain at the
        outer edge of the forest and dense forest at its
        heart.</p><p>@UUID[Compendium.pf-content.pf-rules.JournalEntry.YZZd4279nKAlIWdy]{3.2.2.1.
        Forest Fires}</p><p>The table below describes in general terms how
        likely it is that a given square has a terrain element in
        it.</p><table><thead><tr><td>Category of Forest</td></tr><tr><td>
        </td><td>Sparse</td><td>Medium</td><td>Dense</td></tr></thead><tbody><tr><td>Typical
        trees</td><td>50%</td><td>70%</td><td>80%</td></tr><tr><td>Massive
        trees</td><td>—</td><td>10%</td><td>20%</td></tr><tr><td>Light
        undergrowth</td><td>50%</td><td>70%</td><td>50%</td></tr><tr><td>Heavy
        undergrowth</td><td>—</td><td>20%</td><td>50%</td></tr></tbody></table><h2>Trees</h2><p>The
        most important terrain element in a forest is the trees, obviously. A
        creature standing in the same square as a tree gains partial cover,
        which grants a +2 bonus to Armor Class and a +1 bonus on Reflex saves.
        The presence of a tree doesn’t otherwise affect a creature’s fighting
        space, because it’s assumed that the creature is using the tree to its
        advantage when it can. The trunk of a typical tree has AC 4, hardness 5,
        and 150 hp. A DC 15 Climb check is sufficient to climb a tree. Medium
        and dense forests have massive trees as well. These trees take up an
        entire square and provide cover to anyone behind them. They have AC 3,
        hardness 5, and 600 hp. Like their smaller counterparts, it takes a DC
        15 Climb check to climb them.</p><h2>Undergrowth</h2><p>Vines, roots,
        and short bushes cover much of the ground in a forest. A space covered
        with light undergrowth costs 2 squares of movement to move into, and
        provides concealment. Undergrowth increases the DC of Acrobatics and
        Stealth checks by 2 because the leaves and branches get in the way.
        Heavy undergrowth costs 4 squares of movement to move into and provides
        concealment with a 30% miss chance (instead of the usual 20%). It
        increases the DC of Acrobatics checks by 5. Heavy undergrowth is easy to
        hide in, granting a +5 circumstance bonus on Stealth checks. Running and
        charging are impossible. Squares with undergrowth are often clustered
        together. Undergrowth and trees aren’t mutually exclusive; it’s common
        for a 5-foot square to have both a tree and undergrowth.</p><h2>Forest
        Canopy</h2><p>It’s common for elves and other forest dwellers to live on
        raised platforms far above the surface floor. These wooden platforms
        often have rope bridges between them. To get to the treehouses,
        characters ascend the trees’ branches (Climb DC 15), use rope ladders
        (Climb DC 0), or take pulley elevators (which can be made to rise a
        number of feet equal to a Strength check, made each round as a
        full-round action). Creatures on platforms or branches in a forest
        canopy are considered to have cover when fighting creatures on the
        ground, and in medium or dense forests they have concealment as
        well.</p><h2>Other Forest Terrain Elements</h2><p>Fallen logs generally
        stand about 3 feet high and provide cover just as low walls do. They
        cost 5 feet of movement to cross. Forest streams average 5 to 10 feet
        wide and no more than 5 feet deep. Pathways wind through most forests,
        allowing normal movement and providing neither cover nor concealment.
        These paths are less common in dense forests, but even unexplored
        forests have occasional game trails.</p><h2>Stealth and Detection in a
        Forest</h2><p>In a sparse forest, the maximum distance at which a
        Perception check for detecting the nearby presence of others can succeed
        is 3d6 × 10 feet. In a medium forest, this distance is 2d8 × 10 feet,
        and in a dense forest it is 2d6 × 10 feet.</p><p>Because any square with
        undergrowth provides concealment, it’s usually easy for a creature to
        use the Stealth skill in the forest. Logs and massive trees provide
        cover, which also makes hiding possible.</p><p>The background noise in
        the forest makes Perception checks that rely on sound more difficult,
        increasing the DC of the check by 2 per 10 feet, not 1.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text

