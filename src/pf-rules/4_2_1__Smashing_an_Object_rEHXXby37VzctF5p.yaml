_id: rEHXXby37VzctF5p
_key: '!journal!rEHXXby37VzctF5p'
folder: SorUvhSclTosG5ZR
name: 4.2.1. Smashing an Object
pages:
  - _id: k5brT0CyCe6uCugm
    _key: '!journal.pages!rEHXXby37VzctF5p.k5brT0CyCe6uCugm'
    image: {}
    name: 4.2.1. Smashing an Object
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.kAJexDuJ2IAXZODj]{(Index)
        Exploration}</p><p>@Compendium[pf-content.pf-rules.1EvBiiUnVOT4cUbT]{4.2.
        Breaking and
        Entering}</p><ul><li><strong>@Compendium[pf-content.pf-rules.rEHXXby37VzctF5p]{4.2.1.
        Smashing an
        Object}</strong></li><li>@Compendium[pf-content.pf-rules.e7snRWHn5Dxl7FNp]{4.2.2.
        Breaking Items}</li></ul><hr /><p><strong>Source</strong>: PRPG Core
        Rulebook pg. 173</p><p>Smashing a weapon or shield with a slashing or
        bludgeoning weapon is accomplished with the sunder combat maneuver (see
        Chapter 8). Smashing an object is like sundering a weapon or shield,
        except that your combat maneuver check is opposed by the object’s AC.
        Generally, you can smash an object only with a bludgeoning or slashing
        weapon.</p><h2>Armor Class</h2><p>Objects are easier to hit than
        creatures because they don’t usually move, but many are tough enough to
        shrug off some damage from each blow. An object’s Armor Class is equal
        to 10 + its size modifier (see Table 7–11) + its Dexterity modifier. An
        inanimate object has not only a Dexterity of 0 (–5 penalty to AC), but
        also an additional –2 penalty to its AC. Furthermore, if you take a
        full-round action to line up a shot, you get an automatic hit with a
        melee weapon and a +5 bonus on attack rolls with a ranged
        weapon.</p><h2>Hardness</h2><p>Each object has hardness—a number that
        represents how well it resists damage. When an object is damaged,
        subtract its hardness from the damage. Only damage in excess of its
        hardness is deducted from the object’s hit points (see Table 7–12, Table
        7–13, and Table 7–14).</p><h2>Hit Points</h2><p>An object’s hit point
        total depends on what it is made of and how big it is (see Table 7–12,
        Table 7–13, and Table 7–14). Objects that take damage equal to or
        greater than half their total hit points gain the broken condition (see
        Appendix 2). When an object’s hit points reach 0, it’s
        ruined.</p><p>Very large objects have separate hit point totals for
        different sections.</p><h2>Energy Attacks</h2><p>Energy attacks deal
        half damage to most objects. Divide the damage by 2 before applying the
        object’s hardness. Some energy types might be particularly effective
        against certain objects, subject to GM discretion. For example, fire
        might do full damage against parchment, cloth, and other objects that
        burn easily. Sonic might do full damage against glass and crystal
        objects.</p><h2>Ranged Weapon Damage</h2><p>Objects take half damage
        from ranged weapons (unless the weapon is a siege engine or something
        similar). Divide the damage dealt by 2 before applying the object’s
        hardness.</p><h2>Ineffective Weapons</h2><p>Certain weapons just can’t
        effectively deal damage to certain objects. Likewise, most melee weapons
        have little effect on stone walls and doors, unless they are designed
        for breaking up stone, such as a pick or
        hammer.</p><h2>Immunities</h2><p>Objects are immune to nonlethal damage
        and to critical hits.</p><h2>Magic Armor, Shields, and
        Weapons</h2><p>Each +1 of enhancement bonus adds 2 to the hardness of
        armor, a weapon, or a shield, and +10 to the item’s hit
        points.</p><h2>Vulnerability to Certain Attacks</h2><p>Certain attacks
        are especially successful against some objects. In such cases, attacks
        deal double their normal damage and may ignore the object’s
        hardness.</p><h2>Damaged Objects</h2><p>A damaged object remains
        functional with the broken condition until the item’s hit points are
        reduced to 0, at which point it is destroyed.</p><p>Damaged (but not
        destroyed) objects can be repaired with the Craft skill and a number of
        spells.</p><h2>Saving Throws</h2><p>Nonmagical, unattended items never
        make saving throws. They are considered to have failed their saving
        throws, so they are always fully affected by spells and other attacks
        that allow saving throws to resist or negate. An item attended by a
        character (being grasped, touched, or worn) makes saving throws as the
        character (that is, using the character’s saving throw
        bonus).</p><p>Magic items always get saving throws. A magic item’s
        Fortitude, Ref lex, and Will save bonuses are equal to 2 + half its
        caster level. An attended magic item either makes saving throws as its
        owner or uses its own saving throw bonus, whichever is
        better.</p><h2>Animated Objects</h2><p>Animated objects count as
        creatures for purposes of determining their Armor Class (do not treat
        them as inanimate objects).</p><h2>Table 7-11: Size and Armor Class of
        Objects</h2><table><thead><tr><td>Size</td><td>AC
        Modifier</td></tr></thead><tbody><tr><td>Colossal</td><td>-8</td></tr><tr><td>Gargantuan</td><td>-4</td></tr><tr><td>Huge</td><td>-2</td></tr><tr><td>Large</td><td>-1</td></tr><tr><td>Medium</td><td>+0</td></tr><tr><td>Small</td><td>+1</td></tr><tr><td>Tiny</td><td>+2</td></tr><tr><td>Diminutive</td><td>+4</td></tr><tr><td>Fine</td><td>+8</td></tr></tbody></table><h2>Table
        7-12: Common Armor, Weapon, and Shield Hardness and Hit
        Points</h2><table><thead><tr><td>Weapon or
        Shield</td><td>Hardness<sup>1</sup></td><td>Hit Points<sup>2,
        3</sup></td></tr></thead><tbody><tr><td>Light
        blade</td><td>10</td><td>2</td></tr><tr><td>One-handed
        blade</td><td>10</td><td>5</td></tr><tr><td>Two-handed
        blade</td><td>10</td><td>10</td></tr><tr><td>Light metal-hafted
        weapon</td><td>10</td><td>10</td></tr><tr><td>One-handed metal-hafted
        weapon</td><td>10</td><td>20</td></tr><tr><td>Light hafted
        weapon</td><td>5</td><td>2</td></tr><tr><td>One-handed hafted
        weapon</td><td>5</td><td>5</td></tr><tr><td>Two-handed hafted
        weapon</td><td>5</td><td>10</td></tr><tr><td>Projectile
        weapon</td><td>5</td><td>5</td></tr><tr><td>Armor</td><td>Special<sup>4</sup></td><td>armor
        bonus ×
        5</td></tr><tr><td>Buckler</td><td>10</td><td>5</td></tr><tr><td>Light
        wooden shield</td><td>5</td><td>7</td></tr><tr><td>Heavy wooden
        shield</td><td>5</td><td>15</td></tr><tr><td>Light steel
        shield</td><td>10</td><td>10</td></tr><tr><td>Heavy steel
        shield</td><td>10</td><td>20</td></tr><tr><td>Tower
        shield</td><td>5</td><td>20</td></tr></tbody></table><p>1 Add +2 for
        each +1 enhancement bonus of magic items.<br />2 The hp value given is
        for Medium armor, weapons, and shields. Divide by 2 for each size
        category of the item smaller than Medium, or multiply it by 2 for each
        size category larger than Medium.<br />3 Add 10 hp for each +1
        enhancement bonus of magic items.<br />4 Varies by material; see Table
        7–13: Substance Hardness and Hit Points.</p><h2>Table 7-13: Substance
        Hardness and Hit
        Points</h2><table><thead><tr><td>Substance</td><td>Hardness</td><td>Hit
        Points</td></tr></thead><tbody><tr><td>Glass</td><td>1</td><td>1/in. of
        thickness</td></tr><tr><td>Paper or cloth</td><td>0</td><td>2/in. of
        thickness</td></tr><tr><td>Rope</td><td>0</td><td>2/in. of
        thickness</td></tr><tr><td>Ice</td><td>0</td><td>3/in. of
        thickness</td></tr><tr><td>Leather or hide</td><td>2</td><td>5/in. of
        thickness</td></tr><tr><td>Wood</td><td>5</td><td>10/in. of
        thickness</td></tr><tr><td>Stone</td><td>8</td><td>15/in. of
        thickness</td></tr><tr><td>Iron or steel</td><td>10</td><td>30/in. of
        thickness</td></tr><tr><td>Mithral</td><td>15</td><td>30/in. of
        thickness</td></tr><tr><td>Adamantine</td><td>20</td><td>40/in. of
        thickness</td></tr></tbody></table><h2>Table 7-14: Object Hardness and
        Hit Points</h2><table><thead><tr><td>Object</td><td>Hardness</td><td>Hit
        Points</td><td>DC</td></tr></thead><tbody><tr><td>Rope (1 in.
        diameter)</td><td>0</td><td>2</td><td>23</td></tr><tr><td>Simple wooden
        door</td><td>5</td><td>10</td><td>13</td></tr><tr><td>Small
        chest</td><td>5</td><td>1</td><td>17</td></tr><tr><td>Good wooden
        door</td><td>5</td><td>15</td><td>18</td></tr><tr><td>Treasure
        chest</td><td>5</td><td>15</td><td>23</td></tr><tr><td>Strong wooden
        door</td><td>5</td><td>20</td><td>23</td></tr><tr><td>Masonry wall (1
        ft. thick)</td><td>8</td><td>90</td><td>35</td></tr><tr><td>Hewn stone
        (3 ft.
        thick)</td><td>8</td><td>540</td><td>50</td></tr><tr><td>Chain</td><td>10</td><td>5</td><td>26</td></tr><tr><td>Manacles</td><td>10</td><td>10</td><td>26</td></tr><tr><td>Masterwork
        manacles</td><td>10</td><td>10</td><td>28</td></tr><tr><td>Iron door (2
        in. thick)</td><td>10</td><td>60</td><td>28</td></tr></tbody></table>
      format: 1
    title:
      level: 1
      show: true
    type: text

