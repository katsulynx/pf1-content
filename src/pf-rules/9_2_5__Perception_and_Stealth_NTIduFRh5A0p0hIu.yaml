_id: NTIduFRh5A0p0hIu
_key: '!journal!NTIduFRh5A0p0hIu'
folder: lnmu98MzhpeHCLdt
name: 9.2.5. Perception and Stealth
pages:
  - _id: 8ObBIF0HFoVvCwni
    _key: '!journal.pages!NTIduFRh5A0p0hIu.8ObBIF0HFoVvCwni'
    image: {}
    name: 9.2.5. Perception and Stealth
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.zUOuZVJH1nKufQSo]{(Index)
        Skills}</p><p>@Compendium[pf-content.pf-rules.EJtwDpvYymcGqQZk]{9.2.
        Skills in
        Conflict}</p><ul><li>@Compendium[pf-content.pf-rules.biGV1a41yIiORN2A]{9.2.1.
        Bluff}</li><li>@Compendium[pf-content.pf-rules.sNy0QWGaxnccWVTz]{9.2.2.
        Diplomacy}</li><li>@Compendium[pf-content.pf-rules.NzqehppEukjnQUfy]{9.2.3.
        Disguise}</li><li>@Compendium[pf-content.pf-rules.frhg7KrHN2tYhhkM]{9.2.4.
        Intimidate}</li><li><strong>@Compendium[pf-content.pf-rules.NTIduFRh5A0p0hIu]{9.2.5.
        Perception and
        Stealth}</strong></li><li>@Compendium[pf-content.pf-rules.B1ULzO6COuN6ILPA]{9.2.6.
        Sense
        Motive}</li><li>@Compendium[pf-content.pf-rules.92nmC7Usb2puToet]{9.2.7.
        Replacing Opposed Rolls}</li></ul><hr /><p><strong>Source</strong><em>
        Ultimate Intrigue pg.
        187</em></p><p><em>@UUID[Compendium.pf1.pf1e-rules.JournalEntry.x175kVqUfLGPt8tC.JournalEntryPage.2h6hz5AkTDxKPFxr]{Perception}</em></p><p><em>@UUID[Compendium.pf1.pf1e-rules.JournalEntry.x175kVqUfLGPt8tC.JournalEntryPage.wRWHk7tCUHR99PzD]{Stealth}</em></p><p>Since
        Perception is the skill that determines what a character sees, hears,
        and senses in the game world, it is no wonder that it’s often considered
        the most important skill in the game. Stealth and Perception often
        oppose one another, and the two of them together can be difficult to
        adjudicate.</p><p><strong>Active and Automatic Perception</strong>:
        There are two ways Perception checks happen in the game. The first way
        is automatic and reactive. Certain stimuli automatically call for a
        Perception check, such as a creature using Stealth (which calls for an
        opposed Perception check), or the sounds of combat or talking in the
        distance. The flip side is when a player actively calls for a Perception
        check because her PC is intentionally searching for something. This
        always takes at least a move action, but often takes significantly
        longer. The Core Rulebook doesn’t specify what area a PC can actively
        search, but for a given Perception check it should be no larger than a
        10-foot-by-10-foot area, and often a smaller space if that area is
        cluttered. For instance, in an intrigue-based game, it is fairly common
        to look through a filing cabinet full of files. Though the cabinet
        itself might fill only a 5-foot-by-5-foot area, the number of files
        present could cause a search to take a particularly long
        time.</p><p><strong>Precise and Imprecise Senses</strong>: Since
        Perception covers all senses, it is important to distinguish which of
        those senses count as observing a creature that is using Stealth. Some
        senses are more precise than others. Imprecise senses allow a creature
        to pinpoint the location of another creature, but they don’t allow for
        the use of targeted effects, and attacks against those creatures are
        subject to miss chances from concealment. A few examples of imprecise
        senses are hearing, scent, blindsense, and tremorsense. A sense is
        precise if it allows the creature to use targeted effects on creatures
        and objects it senses, and to attack enemies without suffering a miss
        chance from concealment.</p><p>This includes vision, touch, blindsight,
        and lifesense. Precise senses allow the creature to pinpoint an enemy’s
        location. When a creature uses a precise sense to observe an enemy, that
        enemy is unable to use Stealth against the observer unless it creates a
        distraction first, or has a special ability allowing it to do so. Senses
        other than the listed ones count as precise or imprecise at the GM’s
        discretion. A creature might have a limited form of a sense that makes
        it too weak to count as precise, such as a beast with primitive eyes
        that has difficulty seeing a creature that isn’t
        moving.</p><p><strong>Cover and Concealment for Stealth</strong>: The
        reason a character usually needs cover or concealment to use Stealth is
        tied to the fact that characters can’t use Stealth while being observed.
        A sneaking character needs to avoid all of an opponent’s precise senses
        in order to use Stealth, and for most creatures, that means vision.
        Effects such as blur and displacement, which leave a clear visual of the
        character within the perceiving character’s vision, aren’t sufficient to
        use Stealth, but a shadowy area or a curtain work nicely, for example.
        The hide in plain sight class ability allows a creature to use Stealth
        while being observed and thus avoids this whole situation. As the Core
        Rulebook mentions, a sneaking character can come out of cover or
        concealment during her turn, as long as she doesn’t end her turn where
        other characters are directly observing her.</p><p><strong>States of
        Awareness</strong>: In general, there are four states of awareness that
        a creature can have with regard to another creature using
        Stealth.</p><p><strong>Unaware</strong>: On one end of the spectrum, a
        sneaking creature can succeed at Stealth well enough that the other
        creature isn’t even aware that the creature is present. This state
        allows the sneaking creature to use abilities such as the vigilante’s
        startling appearance. The Stealth skill description in the Core Rulebook
        says that perceiving creatures that fail to beat a sneaking character’s
        Stealth check result are not aware of the sneaking character, but that
        is different from being totally unaware. This is also true of a creature
        that has previously been made aware of the creature’s presence or
        location (see below) but is currently unable to observe the sneaking
        creature. In those cases, the sneaking creature can’t use abilities such
        as startling presence.</p><p><strong>Aware of Presence</strong>: The
        next state is when the perceiving creature is aware of the sneaking
        creature’s presence, though not of anything beyond that. This is the
        state that happens when an invisible creature attacks someone and then
        successfully uses Stealth so the perceiving creature doesn’t know where
        the attacker moved, or when a sniper succeeds at her Stealth check to
        snipe. A perceiving creature that becomes aware of a hidden creature’s
        presence will still be aware of its presence at least until the danger
        of the situation continues, if not longer (though memory-altering magic
        can change this).</p><p><strong>Aware of Location</strong>: The next
        state is awareness of location. This happens when a perceiving character
        uses an imprecise sense, such as hearing or tremorsense, to discover
        what square a hidden or invisible creature
        inhabits.</p><p><strong>Observing</strong>: The final state is when the
        perceiving character is able to directly observe the sneaking character
        with a precise sense, such as vision. This is generally the result when
        the perceiving character rolls higher on its opposed Perception check
        than the sneaking character’s Stealth result while also having line of
        sight to the sneaking character and the ability to see through any sort
        of invisibility or other tricks the sneaking character might be
        using.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text

