_id: kvl8JOILza5HEj7N
_key: '!journal!kvl8JOILza5HEj7N'
folder: CMTuXqZ6Ywxqk9Ar
name: 7.1.5.13. Casting Time
pages:
  - _id: QdLBiWyiQmjySpfr
    _key: '!journal.pages!kvl8JOILza5HEj7N.QdLBiWyiQmjySpfr'
    image: {}
    name: 7.1.5.13. Casting Time
    src: null
    system: {}
    text:
      content: >-
        <p>@Compendium[pf-content.pf-rules.dS0UkKOINqI2iTDX]{(Index)
        Magic}</p><p>@Compendium[pf-content.pf-rules.zCeqoMSKdZ8r4EPd]{7.1.
        Mastering
        Magic}</p><ul><li><p>@Compendium[pf-content.pf-rules.xdyGkidQGN7vYL1E]{7.1.5.
        Designing
        Spells}</p><ul><li>@Compendium[pf-content.pf-rules.7Lc81tg2g1Q6TMPo]{7.1.5.01.
        The Golden
        Rule}</li><li>@Compendium[pf-content.pf-rules.DZBAa9axA5Gv4el0]{7.1.5.02.
        Spell
        Terminology}</li><li>@Compendium[pf-content.pf-rules.dbb5pwAitIa3cqqj]{7.1.5.03.
        Intended
        Level}</li><li>@Compendium[pf-content.pf-rules.1DkXvcE3KMmBww9Z]{7.1.5.04.
        Function}</li><li>@Compendium[pf-content.pf-rules.Xu1jyIRMERPFh8X6]{7.1.5.05.
        Spell
        Research}</li><li>@Compendium[pf-content.pf-rules.fF9HpImlhp8oNX5Q]{7.1.5.06.
        Spell
        Damage}</li><li>@Compendium[pf-content.pf-rules.zrMFTIHfzbQTTwZ1]{7.1.5.07.
        Target}</li><li>@Compendium[pf-content.pf-rules.wsF1rdV2ACkYU3Su]{7.1.5.08.
        Damage
        Caps}</li><li>@Compendium[pf-content.pf-rules.9TyMr3XedNUcWVzB]{7.1.5.09.
        Range}</li><li>@Compendium[pf-content.pf-rules.KlR2PDab6uknvopJ]{7.1.5.10.
        Duration}</li><li>@Compendium[pf-content.pf-rules.x0E7IZ5imhQ5srrf]{7.1.5.11.
        Saving
        Throw}</li><li>@Compendium[pf-content.pf-rules.RRG1A0rBL44Qeskg]{7.1.5.12.
        Spell
        Resistance}</li><li><strong>@Compendium[pf-content.pf-rules.kvl8JOILza5HEj7N]{7.1.5.13.
        Casting
        Time}</strong></li><li>@Compendium[pf-content.pf-rules.wb9tYgBlWX8peqFl]{7.1.5.14.
        Components}</li><li>@Compendium[pf-content.pf-rules.PCvwd6PaQH7mD7o6]{7.1.5.15.
        School}</li><li>@Compendium[pf-content.pf-rules.dmUzb6b2BspbBdDp]{7.1.5.16.
        Bonus
        Type}</li><li>@Compendium[pf-content.pf-rules.gh6sxSeK1RNLLFoN]{7.1.5.17.
        Description}</li><li>@Compendium[pf-content.pf-rules.q6Xva2npOy7fVL42]{7.1.5.18.
        Hierarchy of Attack
        Effects}</li><li>@Compendium[pf-content.pf-rules.s6CWKZDo0zQeRXMJ]{7.1.5.19.
        Depletable
        Statistics}</li><li>@Compendium[pf-content.pf-rules.r8zYCFuq59NEtjkz]{7.1.5.20.
        Core is
        King}</li><li>@Compendium[pf-content.pf-rules.aVUL2wrJszAUzNMK]{7.1.5.21.
        Multipurpose
        Spells}</li><li>@Compendium[pf-content.pf-rules.Ak9Ws3Z16GY6QSoH]{7.1.5.22.
        Choosing
        Descriptors}</li><li>@Compendium[pf-content.pf-rules.baSlaM24kwB68nuf]{7.1.5.23.
        Benchmarks}</li></ul></li></ul><hr /><p><strong>Source</strong>
        <em>Ultimate Magic pg. 132</em></p><p>Almost all spells meant to be cast
        in combat should have a casting time of “1 standard action.” Avoid the
        temptation to invent spells with a casting time of “1 move action,” “1
        swift action,” or “1 immediate action,” as that’s just a cheesy way for
        spellcasters to be able to cast two spells in 1 round, and there’s
        already a mechanism for that: the Quicken Spell feat. Making combat
        spells with faster casting times devalues the Quicken Spell feat; even
        if you design the spell to be similar to a quickened spell, including
        the +4 level boost, it steals from casters who actually learn that feat,
        and your spell would become a common combo for high-level casters who
        can afford extra spells of that level. For example, if you create a
        5th-level quickened magic missile spell that acts just like a magic
        missile spell with the Quicken Spell feat, any 14th-level wizard (who
        has at least three 5th-level spells available) is going to be tempted to
        learn this spell just because it allows him to add 5d4+5 extra points of
        damage to any high-level combat spell he casts, which is a way to get
        around the spell-damage cap. Furthermore, allowing spellcasters to
        routinely cast two spells per round means they tend to use up their
        spells more quickly and push their allies to camp and rest rather than
        continue exploring.</p><p>Spells that summon creatures to help in combat
        should have a casting time of “1 round.” This is to give a reasonable
        action cost for a character casting the spell. If the caster could
        summon a monster using a standard action and have it act that same
        round, it’s like the spell didn’t cost him any actions at
        all.</p><p>Spells that call an outsider to serve for more than a few
        rounds (such as planar ally and planar binding) should have a casting
        time of 10 minutes; more powerful spells may have even longer casting
        times. Note that gate can be used to call creatures and only has a
        casting time of 1 standard action, but when used this way, it requires a
        10,000 gp material component, so that faster casting time doesn’t come
        cheaply.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text

